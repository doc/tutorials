
mm = 1e-3;
deg = 180/Pi;

/*
 Expected inductance of a rectilinear 1mm radius wire: 4 nH/cm 

 git/documentation/models/Inductor/magstadyn_av_js0_3d.pro 
 */

DefineConstant[
  NumWires = {1, Name "Parameters/00Number of wires", 
			  Choices {1="1", 2="2", 3="3"}, Visible 1}
  
  Flag_Thin = {1, Name "Parameters/01Thin wires", 
    Choices {0,1}, Visible 1}
  Flag_RegSleeve = {1, Name "Parameters/02Regular sleeves", 
    Choices {0,1}, Visible Flag_Thin}
  
  Flag_SemiAnalytic = {1, Name "Parameters/03Semi-analytic approach", 
    Choices {0,1}, Visible Flag_Thin}
  
  Flag_Stranded = {0, Name "Parameters/04Stranded conductors", 
                   Choices {0,1}, Visible !Flag_SemiAnalytic || !Flag_Thin}
  // Flag_Dual = {0, Name "Parameters/05Dual approach", 
  //   Choices {0,1}, Visible Flag_SemiAnalytic && Flag_Thin}
  Flag_U = {0, Name "Parameters/06Impose U", Choices {0,1}, Visible 1}
  
  WireRadius   = {1, Name "Parameters/10Wire radius [mm]"}
  MeshSizeWire = {0.25, Name "Parameters/11Mesh size in wire [mm]", 
    Visible !Flag_Thin}
  MeshSizeThinWire = {2, Name "Parameters/12Mesh size on thin wire [mm]", 
    Visible Flag_Thin}
  //SleeveRadius = {2, Name "Parameters/13Sleeve radius [mm]", Visible Flag_RegSleeve}
  box = {100*mm, Name "Parameters/5box half-width [m]"}
  
  Flag_Maps = {1, Name "Input/2Display maps", Choices {0,1}, Visible 1}
  
  LogFreq = {4,  Min 1, Max 8, Step 0.25, Name "Input/4Log of frequency"}
  Freq = 10^LogFreq
  
  rw = WireRadius*mm 
  rs = MeshSizeThinWire*mm
  A_c = Pi*rw^2 // wire cross section
  rout = box
  Lz = 10*mm
  NbDivision = 10
  

  /* 'WireRadius' is the actual radius of the wire.
     'MeshSizeWire' is the imposed mesh size at the nodes of the wire,
     and thus also the practical approximative value of the radius of the sleeve.
     The 'rs' and 'rw' parameters are assumed identical for all wires.
   */
];

If( Flag_SemiAnalytic ) 
  SetNumber("Parameters/01Thin wires", 1);
  Flag_Thin=1;
EndIf


Xlocs() = { 0, 5*mm, -5*mm};
Ylocs() = { 0, 0, 0};
For i In {1:NumWires}
  DefineConstant[
	WX~{i} = { Xlocs(i-1), // place wires symmetrically around x=0
			   Name Sprintf("Parameters/Wire %g/0X position [m]", i), Closed }
    WY~{i} = { Ylocs(i-1), Name Sprintf("Parameters/Wire %g/0Y position [m]", i) }
    WI~{i} = { 1, Name Sprintf("Parameters/Wire %g/2Current [A]", i) }
    WP~{i} = { 0*NumWires*(i-1),
      Name Sprintf("Parameters/Wire %g/3Phase [deg]", i) }
    ];
EndFor
