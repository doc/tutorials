Include "w2d_common.pro";

Struct S::SOLVER [ Enum, mumps, gmres, fgmres,bcgs ];
DefineConstant[
  FileId = "" 
  DataDir     = StrCat["data/"               , FileId] 
  ElekinDir   = StrCat["Elekin_Line_Const/"  , FileId]
  MagDynDir   = StrCat["MagDyn_Line_Const/"  , FileId]
  FullWaveDir = StrCat["FullWave_Line_Const/", FileId]
  type_solver = {
    S::SOLVER.mumps, 
    Choices { 
      S::SOLVER.mumps ="mumps",
      S::SOLVER.gmres="gmres",
      S::SOLVER.fgmres= "fgmres",
      S::SOLVER.bcgs="bcgs"
    },
    Name "Solver/type_solver"
  }
];

DefineConstant[
  R_ = {"Dynamic", Name "GetDP/1ResolutionChoices", Visible 0},
  C_ = {"-sol -pos", Name "GetDP/9ComputeCommand", Visible 0},
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 0}
  ResDir = "Res/" 
];


Group{
  // Geometrical regions
  AIR = Region[ 1 ];
  INF = Region[ 2 ];

  VWIRES = Region[ {} ];
  LWIRES = Region[ {} ];
  For i In {1:NumWires}
    LWIRE~{i} = Region[ (50+i) ];
    LWIRES += Region[ LWIRE~{i} ];
    If( Flag_Thin == 0 )
      VWIRE~{i} = Region[ (10+i) ];
      VWIRES += Region[ VWIRE~{i} ];
      SWIRE~{i} = Region[ {} ];
    Else
      VWIRE~{i} = Region[ {} ];
      SWIRE~{i} = ElementsOf[ AIR, OnOneSideOf LWIRE~{i} ];
    EndIf
  EndFor

  // Abstract regions
  // Cette formulation peut contenir des conducteurs volumiques Vol_C
  // et lin�iques lVol_C.

  If( Flag_Thin == 0  )
    Vol_C   = Region[ VWIRES ];
    Vol_CC  = Region[ AIR ];
    lVol_C  = Region[ {} ];
  Else
    Vol_C   = Region[ {} ];
    Vol_CC  = Region[ {VWIRES, AIR} ];
    lVol_C  = Region[ LWIRES ];
  EndIf

  Vol_nu = Region[ {Vol_C, Vol_CC, lVol_C} ];
  Sur_Dirichlet_a = Region[ INF ];

  // 3D
  If( Flag_3D ) 
    Sur_Dirichlet_a += Region[ { BOT, TOP} ];
  EndIf
  Sur_Complete_Tree = Region[ { lVol_C, Sur_Dirichlet_a } ];
  Electrodes = Region[ {} ];   

  Dom_Hcurl_a = Region[ Vol_nu ];
  Dom_Hthin_a = ElementsOf[ Vol_nu, OnOneSideOf lVol_C ]; // Sleeve
  Dom_Hgrad_u = Region[ Vol_C ];
  Dom_Hregion_i = Region[ lVol_C ];

  // For integration on the volume elements (excluding line elements) of the sleeve
  Vol_Sleeve = ElementsOf[ {Vol_CC}, OnOneSideOf lVol_C ]; 
}

Function{
  omega = 2*Pi*Freq;
  mu0 = Pi*4e-7;
  mur = 1.;
  sigma = 5.96e7; 

  i[] = Complex[0,1];

  mu[] = mu0;
  nu[] = 1/mu[];
  sigma[] = sigma;
  skin_depth = Sqrt[ 2/(omega*sigma*mu0*mur) ];
  tau[] = Complex[-1,1]/skin_depth*rw;
  J0[] = JnComplex[0,$1];
  J1[] = JnComplex[1,$1];
  dJ1[] = JnComplex[0,$1]-JnComplex[1,$1]/$1;

  For i In {1:NumWires}
    // Current density
    J[ Region[ {VWIRE~{i}, LWIRE~{i}} ] ] = 
    Vector[ 0, 0, WI~{i}/A_c * Exp[ i[]*WP~{i}*deg ] ];

    // local radial coordinate with origin on wire i
    R~{i}[] = Hypot[ X[]-WX~{i} , Y[]-WY~{i} ];
  EndFor

  // shape function of the current, wI[r] 
  wI[] = tau[] * J0[tau[]*$1/rw] / J1[tau[]] /(2*A_c); 

  //radial analytical solutions with a zero flux condition imposed at $1(=r)=rs
  Analytic_A[] = // per Amp
                (($1>rw) ?  mu0/(2*Pi) * Log[rs/$1] :
                  mu0/(2*Pi) * Log[rs/rw] + i[]*( wI[$1]-wI[rw] )/(sigma*omega) ); 
  AnalyticStatic_A[] = mu0/(2*Pi) * // per Amp
                (($1>rw) ? Log[rs/$1] : Log[rs/rw] + mur*(1-($1/rw)^2)/2 );
 
  // Impedance of thin wire p.u. length
  R_DC = 1./(sigma*A_c);
  Analytic_R[] = Re[ wI[rw]/sigma ];
  Analytic_L[] = mu0/(2*Pi) * Log[rs/rw] + Re[ wI[rw]/(i[]*omega*sigma) ];

  If( NumWires == 1 )
    Exact_A[] = WI_1*(Analytic_A[R_1[]] + mu0/(2*Pi) * Log[rout/rs]) ;
    Correction_A[] = ((R_1[]<rs) ? WI_1*Analytic_A[R_1[]] : 0 ) ;
  EndIf
  If ( NumWires == 2 )
    Exact_A[] = WI_1*Analytic_A[R_1[]] + WI_2*Analytic_A[R_2[]] 
    + (WI_1+WI_2)*mu0/(2*Pi) * NumWires * Log[rout/rs] ;
    Correction_A[] = ((R_1[]<rs) ? WI_1*Analytic_A[R_1[]] : 
                     ((R_2[]<rs) ? WI_2*Analytic_A[R_2[]] : 0 ));
  EndIf
  If ( NumWires == 3 )
    Exact_A[] = WI_1*Analytic_A[R_1[]] + WI_2*Analytic_A[R_2[]] 
    + WI_3*Analytic_A[R_3[]] + (WI_1+WI_2+WI_3)*mu0/(2*Pi) * NumWires * Log[rout/rs] ;
    Correction_A[] = ((R_1[]<rs) ? WI_1*Analytic_A[R_1[]] : 
                     ((R_2[]<rs) ? WI_2*Analytic_A[R_2[]] : 
                     ((R_3[]<rs) ? WI_3*Analytic_A[R_3[]] : 0 )));
  EndIf
}


Jacobian {
  { Name Vol ;
    Case {
      { Region lVol_C ; Jacobian Lin ; Coefficient A_c; }
      { Region All ; Jacobian Vol ; }
    }
  }
}

Integration {
  { Name I1 ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point       ; NumberOfPoints  1 ; }
          { GeoElement Line        ; NumberOfPoints  3 ; }
          { GeoElement Triangle    ; NumberOfPoints  3 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
          { GeoElement Prism       ; NumberOfPoints 21 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
          { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
          { GeoElement Pyramid     ; NumberOfPoints  8 ; }
        }
      }
    }
  }
}

Constraint{
  // 2D

  { Name Hcurl_a_2D_ac; Type Assign;
    Case {
      { Region Region[ Sur_Dirichlet_a ]; Value 0; }      
    }
  }

  { Name Impose_I ;
    Case {
      For i In {1:NumWires}
        { Region VWIRE~{i} ; Value  WI~{i} ; }
        { Region LWIRE~{i} ; Value  WI~{i} ; }
      EndFor
    }
  }

  // 3D
  { Name Hcurl_a_3D_ac; Type Assign;
    Case {
      { Region Region[ Sur_Dirichlet_a ]; Value 0.; }
    }
  }
  { Name GaugeCondition_a; Type Assign;
    Case {
      { Region Vol_CC; SubRegion Sur_Complete_Tree; Value 0.; }
    }
  }
}

FunctionSpace {
  // 2D 

  { Name Hcurl_a_2D; Type Form1P;
    BasisFunction {
      { Name sc; NameOfCoef ac; Function BF_PerpendicularEdge;
        Support Dom_Hcurl_a; Entity NodesOf[All]; }
      { Name sw; NameOfCoef aw; Function BF_PerpendicularEdge;
        Support Dom_Hthin_a; Entity NodesOf[lVol_C]; }
    }
    SubSpace {
      { Name ac ; NameOfBasisFunction { sc } ; }
      { Name aw ; NameOfBasisFunction { sw } ; }
    }
    Constraint {
      { NameOfCoef ac;
        EntityType Auto; NameOfConstraint Hcurl_a_2D_ac; }
    }
  }

  { Name Hgrad_u_2D ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support Dom_Hgrad_u ; Entity Vol_C ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef I ; EntityType GroupsOfNodesOf ; NameOfConstraint Impose_I ; }
    }
  }

  { Name Hregion_i_2D; Type Vector;
    BasisFunction {
      { Name sr; NameOfCoef ir; Function BF_RegionZ;
        Support Dom_Hregion_i; Entity lVol_C; }
    }
    GlobalQuantity {
      { Name Is; Type AliasOf; NameOfCoef ir; }
      { Name Us; Type AssociatedWith; NameOfCoef ir; }
    }
    Constraint {
      { NameOfCoef Is;
        EntityType Region; NameOfConstraint Impose_I; }
    }
  }

  // 3D 
  { Name Hcurl_a_3D; Type Form1;
    BasisFunction {
      { Name sc; NameOfCoef ac; Function BF_Edge;
        Support Dom_Hcurl_a; Entity EdgesOf[All]; }
      { Name sw; NameOfCoef aw; Function BF_Edge;
        Support Dom_Hthin_a; Entity EdgesOf[lVol_C]; }
    }
    SubSpace {
      { Name ac ; NameOfBasisFunction { sc } ; }
      { Name aw ; NameOfBasisFunction { sw } ; }
    }
    Constraint {
      { NameOfCoef ac;
        EntityType Auto; NameOfConstraint Hcurl_a_3D_ac; }
    }
  }

  { Name Hgrad_u_3D; Type Form0; 
    BasisFunction {
      { Name sn; NameOfCoef un; Function BF_Node;
        Support Dom_Hgrad_u; Entity NodesOf[ Vol_C, Not Electrodes]; }
      { Name sn2; NameOfCoef un2; Function BF_GroupOfNodes;
        Support Dom_Hgrad_u; Entity GroupsOfNodesOf[ Electrodes ]; }  
   }
   GlobalQuantity {
      { Name U; Type AliasOf       ; NameOfCoef un2; }
      { Name I; Type AssociatedWith; NameOfCoef un2; }
    }
    Constraint { 
      //{ NameOfCoef U;   EntityType Auto; NameOfConstraint Impose_U; }
      { NameOfCoef I;   EntityType Auto; NameOfConstraint Impose_I; }
    }
  }
}

Formulation {
  { Name MagnetoDynamics; Type FemEquation;
    Quantity {
      { Name a;  Type Local;  NameOfSpace Hcurl_a_2D; }
      { Name ac; Type Local;  NameOfSpace Hcurl_a_2D[ac]; }
      { Name aw; Type Local;  NameOfSpace Hcurl_a_2D[aw]; }

      { Name dv;  Type Local;  NameOfSpace Hgrad_u_2D; }
      { Name U;  Type Global; NameOfSpace Hgrad_u_2D [U]; }
      { Name I;  Type Global; NameOfSpace Hgrad_u_2D [I]; }

      { Name i; Type Local;  NameOfSpace Hregion_i_2D; }
      { Name Is;  Type Global; NameOfSpace Hregion_i_2D [Is]; }
      { Name Us;  Type Global; NameOfSpace Hregion_i_2D [Us]; }
    }

    Equation {

      Integral { [ nu[] * Dof{d ac} , {d ac} ];
        In Vol_nu; Jacobian Vol; Integration I1; }
      Integral { [ -Dof{i}/A_c , {ac} ];
        In lVol_C; Jacobian Vol; Integration I1; }

      Integral { [ nu[] * Dof{d aw} , {d aw} ];
        In Vol_nu; Jacobian Vol; Integration I1; }
      Integral { [-Dof{i}/A_c , {aw} ];
        In lVol_C; Jacobian Vol; Integration I1; }

      /*
      Integral { [ -J[] , {a} ];
        In Vol_C; Jacobian Vol; Integration I1; }
      */

      Integral { DtDof[ sigma[] * Dof{a} , {a} ];
        In Vol_C; Jacobian Vol; Integration I1; }
      Integral { [ sigma[] * Dof{dv} , {a} ];
        In Vol_C; Jacobian Vol; Integration I1; }

      Integral { DtDof[ sigma[] * Dof{a} , {dv} ];
        In Vol_C; Jacobian Vol; Integration I1; }      
      Integral { [ sigma[] * Dof{dv} , {dv} ];
        In Vol_C; Jacobian Vol; Integration I1; }   
      GlobalTerm { [ Dof{I} , {U} ]; In Vol_C; }

      GlobalTerm { [ Dof{Us} , {Is} ]; 
        In lVol_C; }
      GlobalTerm { [ Analytic_R[] * Dof{Is} , {Is} ]; 
        In lVol_C;}
      Integral { DtDof [ Dof{ac}/A_c , {i} ];
        In lVol_C; Jacobian Vol; Integration I1; }

      If( Flag_Thin != 3)
        Integral { DtDof [ -Dof{aw}/A_c , {i} ];
          In lVol_C; Jacobian Vol; Integration I1;}
        GlobalTerm { DtDof [ Analytic_L[] * Dof{Is} , {Is} ]; 
          In lVol_C;}
      EndIf
    }
  }
}

Resolution {
 { Name Dynamic;
    System {
      { Name S; NameOfFormulation MagnetoDynamics;
        Type ComplexValue; Frequency Freq; }
    }
    Operation {
      CreateDir[DataDir];

      // SOLVER OPTION
      If(type_solver == S::SOLVER.gmres)
        SetGlobalSolverOptions[
          "-pc_type ilu -ksp_type gmres -ksp_max_it 1000 -ksp_rtol 1e-6 -ksp_atol 1e-6"];
        ElseIf(type_solver == S::SOLVER.fgmres)
        SetGlobalSolverOptions[
          "-pc_type ilu -ksp_type fgmres -ksp_max_it 1000 -ksp_rtol 1e-6 -ksp_atol 1e-6"];
        ElseIf(type_solver == S::SOLVER.bcgs)
        SetGlobalSolverOptions[
          "-pc_type ilu -ksp_type bcgs -ksp_max_it 1000  -ksp_rtol 1e-6 -ksp_atol 1e-6"];
      EndIf
      SetGlobalSolverOptions["-petsc_prealloc 900"];
      
      Generate[S]; Solve[S]; SaveSolution[S]; 

      PostOperation[integaz];

      If( Flag_Maps == 1 )
        PostOperation[mapaz];
        PostOperation[cutaz];

        DeleteFile["U_full.dat"];
        DeleteFile["U_raw.dat"];
        DeleteFile["U_reg.dat"];
        DeleteFile["U_naive.dat"];
        DeleteFile["joule_full.dat"];
        DeleteFile["joule_raw.dat"];
        DeleteFile["joule_reg.dat"];
        DeleteFile["joule_naive.dat"];
      EndIf

      If( Flag_Thin != 0 )
        For i In {1:NumWires}
          Print[ {i, rw, Sqrt[ $sarea~{i}/Pi], rs, $sarea~{i}, Pi*rs^2, skin_depth, $intby~{i}/$sarea~{i}}, 
            Format "WIRE %2g: rw = %7.3e rs = %7.3e (%7.3e) as = %7.3e (%7.3e) delta = %7.3e bave = %7.3e" ] ;
          //Print[ {$sarea~{i}}, Format "DEBUG %7.3e" ] ;
        EndFor
      EndIf
    }
  }
}

PostProcessing {
  { Name PostProcessings; NameOfFormulation MagnetoDynamics;
    PostQuantity {
      { Name az;
        Value { Local { [ CompZ[{ac}] - (Flag_Thin!=0)*(CompZ[{aw}]-Correction_A[])];
            In  Dom_Hcurl_a; Jacobian Vol; }}}
      { Name acz;
        Value { Local { [ CompZ[ {ac}] ];
            In  Dom_Hcurl_a; Jacobian Vol; }}}
      { Name awz;
        Value { Local { [ CompZ[ {aw}] ];
            In Dom_Hthin_a; Jacobian Vol; }}}
      { Name acwz;
        Value { Local { [ CompZ[ {ac} - {aw} ] ];
            In Dom_Hcurl_a; Jacobian Vol; }}}
      { Name exact;
        Value { Local { [ Exact_A[] ] ;
            In Dom_Hcurl_a; Jacobian Vol; }}}
      { Name corr;
        Value { Local { [ Correction_A[] ] ;
            In Dom_Hcurl_a; Jacobian Vol; }}}
      { Name bcw;
        Value { Local { [ {d ac} - {d aw} ];
            In Dom_Hcurl_a; Jacobian Vol; }}}

      { Name j;
        Value { Local { [ -sigma[] * ( Dt[{a}] + {dv} ) ]; 
            In Vol_C; Jacobian Vol; }}}

      { Name un;
        Value{ Local { [ 1 ] ;
            In Vol_nu; Integration I1 ; Jacobian Vol; }}}  
      { Name surf;
        Value{ Integral{ [ 1 ] ;
            In Vol_nu; Integration I1 ; Jacobian Vol; }}}

      If( Flag_Thin == 0 )
        { Name flux;
          Value { Integral { [ CompZ[ {a} ] / A_c  ]; 
              In Vol_C; Integration I1 ; Jacobian Vol; }}}
        { Name JouleLosses;
          Value { Integral { [ sigma[]*SquNorm[ Dt[{a}] + {dv} ] ] ;
              In Vol_C ; Integration I1 ; Jacobian Vol ; }}}
        { Name U;
          Value { Term { [  Cart2Pol [ {U} ] ]; In Vol_C; }}}
        { Name L;
          Value { Term { [  Im [ {U}/{I}/omega ] ]; In Vol_C; }}}

        Else
        { Name flux; // naive flux
          Value { Integral { [ CompZ[ {ac} ] ]; 
              In lVol_C; Integration I1 ; Jacobian Vol; }}}

        { Name JouleLosses; 
          Value { Term { [ Analytic_R[]*{Is}*{Is} ] ; In lVol_C; }}}
        { Name U;
          Value { Term { [ Cart2Pol [ {Us} ] ] ; In lVol_C; }}}
        { Name L; // only valid with one current 
          Value { Term { [ Im [ {Us}/{Is}/omega ] ] ; In lVol_C; }}}
        
        For i In {1:NumWires}
          { Name area~{i};
            Value{ Integral{ [ 1 ] ;
                In SWIRE~{i}; Integration I1 ; Jacobian Vol; }}}
          { Name intbx~{i}; // integral of b, 1st step for computing b average 
            Value { Integral { [ Norm [ CompX[ {d ac} - {d aw} ] ] ] ; 
                In SWIRE~{i}; Integration I1 ; Jacobian Vol; }}}
          { Name intby~{i}; // integral of b, 1st step for computing b average 
            Value { Integral { [ Norm [ CompY[ {d ac} - {d aw} ] ] ] ; 
                In SWIRE~{i}; Integration I1 ; Jacobian Vol; }}}
        EndFor
      EndIf
    }
  }
}

PostOperation mapaz UsingPost PostProcessings {
  Print [az, OnElementsOf Vol_nu, File "az.pos"];
  Echo[ StrCat["l=PostProcessing.NbViews-1;",
      "View[l].IntervalsType = 3;",
      "View[l].NbIso = 30;",
      "View[l].RaiseZ = 15000;",
      "View[l].NormalRaise = 0;"],
    File "tmp.geo", LastTimeStepOnly];
 
  If(Flag_Thin != 0)
    Print [bcw, OnElementsOf Vol_Sleeve, File "b.pos"];

    Print [acwz, OnElementsOf Vol_nu, File "acz.pos"];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].IntervalsType = 3;",
        "View[l].NbIso = 30;",
        "View[l].RaiseZ = 15000;",
        "View[l].NormalRaise = 0;"],
      File "tmp.geo", LastTimeStepOnly] ;
  EndIf
}


NbPoints = 1000;
Xmax = 0.15*box;

PostOperation cutaz UsingPost PostProcessings {
  // cut of az displayed in GUI
  If( Flag_Thin == 0 )
    Print [ az, OnLine { {-Xmax,0,0} {Xmax,0,0} }{NbPoints},
      Format Gmsh, File "Cut_az.pos" ];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].Name = 'cut az full';",
        "View[l].Axes = 3;",
        "View[l].LineWidth = 3;",
        "View[l].Type = 2;"],
      File "tmp.geo", LastTimeStepOnly];

    Else

    Print [ acz, OnLine { {-Xmax,0,0} {Xmax,0,0} }{NbPoints},
      Format Gmsh, File "Cut_az.pos" ];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].Name = 'cut az not corrected';",
        "View[l].Axes = 3;",
        "View[l].LineWidth = 3;",
        "View[l].Type = 2;"],
      File "tmp.geo", LastTimeStepOnly];

    Print [ az, OnLine { {-Xmax,0,0} {Xmax,0,0} }{NbPoints},
      Format Gmsh, File "Cut_az.pos" ];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].Name = 'cut az corrected';",
        "View[l].Axes = 3;",
        "View[l].LineWidth = 3;",
        "View[l].Type = 2;"],
      File "tmp.geo", LastTimeStepOnly];
  EndIf

  // cuts in txt format for Gnuplot
  Print [ exact, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
    Format SimpleTable, File "Cut_analytic.txt" ];

  If( Flag_Thin == 0 )
    Print [ az, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_fullfem.txt" ];
  Else
    Print [ az, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_thinfem.txt" ];
    Print [ acz, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_ac.txt" ];
    Print [ awz, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_aw.txt" ];
    Print [ acwz, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_acw.txt" ];
    Print [ corr, OnLine { {0,0,0} {Xmax,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_correction.txt" ];
  EndIf
}


PostOperation integaz UsingPost PostProcessings {
  For i In {1:NumWires}

    If( Flag_Thin == 0 )
      Print[ U, OnRegion Vol_C, File > "U_full.dat", Format Table,
        SendToServer Sprintf["}Voltage/Wire %g/1Full",i]];
      Print[ L, OnRegion Vol_C, File > "L_full.dat", Format Table,
        SendToServer Sprintf["}Inductance/Wire %g/1Full",i]];
      Print[ JouleLosses[ VWIRE~{i}], OnGlobal,
        Format Table, File > "joule_full.dat", 
        SendToServer Sprintf["}Joule Losses/Wire %g/1Full",i]];
    EndIf
    
    If( Flag_Thin == 1 )
      Print[ U, OnRegion lVol_C, File > "U_raw.dat", Format Table,
        SendToServer Sprintf["}Voltage/Wire %g/2Raw",i]];
      Print[ L, OnRegion lVol_C, File > "L_raw.dat", Format Table,
        SendToServer Sprintf["}Inductance/Wire %g/2Raw",i]];
      Print[ JouleLosses, OnRegion LWIRE~{i},
        Format Table, File > "joule_raw.dat", 
        SendToServer Sprintf["}Joule Losses/Wire %g/2Raw",i]];
    EndIf
    
    If( Flag_Thin == 2 )
      Print[ U, OnRegion lVol_C, File > "U_reg.dat", Format Table,
        SendToServer Sprintf["}Voltage/Wire %g/3Reg",i]];
      Print[ L, OnRegion lVol_C, File > "L_reg.dat", Format Table,
        SendToServer Sprintf["}Inductance/Wire %g/3Reg",i]];
      Print[ JouleLosses, OnRegion LWIRE~{i},
        Format Table, File > "joule_reg.dat", 
        SendToServer Sprintf["}Joule Losses/Wire %g/3Reg",i]];
    EndIf

    If( Flag_Thin == 3 )
      Print[ U, OnRegion lVol_C, File > "U_naive.dat", Format Table,
        SendToServer Sprintf["}Voltage/Wire %g/4Naive",i]];
      Print[ L, OnRegion lVol_C, File > "L_naive.dat", Format Table,
        SendToServer Sprintf["}Inductance/Wire %g/4naive",i]];
      Print[ JouleLosses, OnRegion LWIRE~{i},
        Format Table, File > "joule_naive.dat", 
        SendToServer Sprintf["}Joule Losses/Wire %g/4Naiv",i]];
    EndIf

    If( Flag_Thin != 0 )
      Print[ surf[SWIRE~{i}], OnGlobal,
        Format Table, File >"zzz", StoreInVariable $sarea~{i}];
      Print[ intby~{i}[SWIRE~{i}], OnGlobal,
        Format Table, File > "zzz", StoreInVariable $intby~{i}];
    EndIf

  EndFor
}


