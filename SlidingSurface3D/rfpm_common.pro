mm = 1.e-3 ;
deg = Pi/180.;

lc = mm * DefineNumber[10, Name "Options/Global mesh size (mm)"];
If(lc <= 0)
  Error("Glabal mesh size cannot be zero. Reset to default.");
  lc = 10 * mm;
EndIf

// Number of elements on the sliding surface in rotation direction
NbrDiv = Floor[100*mm/lc];

ModelAngleMin = 0. ;
ModelAngleMax = 60. ;

//SlidingSurfaceShift = 1.e-5;
SlidingSurfaceShift = 0;
