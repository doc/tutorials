Include "w3d_common.pro";

Struct S::SOLVER [ Enum, mumps, gmres, fgmres, bcgs ];

DefineConstant[
  FileId = ""
  DataDir     = StrCat["data/"               , FileId]
  ElekinDir   = StrCat["Elekin_Line_Const/"  , FileId]
  MagDynDir   = StrCat["MagDyn_Line_Const/"  , FileId]
  FullWaveDir = StrCat["FullWave_Line_Const/", FileId]
  type_solver = {
    S::SOLVER.mumps,
    Choices {
      S::SOLVER.mumps ="mumps",
      S::SOLVER.gmres="gmres",
      S::SOLVER.fgmres= "fgmres",
      S::SOLVER.bcgs="bcgs"
    },
    Name "Solver/type_solver"
  }
];

DefineConstant[
  R_ = {"Dynamic", Name "GetDP/1ResolutionChoices", Visible 0},
  C_ = {"-solve -v2", Name "GetDP/9ComputeCommand", Visible 0},
  P_ = {"", Name "GetDP/2PostOperationChoices", Visible 0}
  ResDir = "Res/"
];


Group{
  // Geometrical regions
  AIR = Region[ 1 ];
  INF = Region[ 2 ];
  SKIN = Region[ 3 ];
  LINTREE = Region[ 4 ];

  VWIRES = Region[ {} ];
  LWIRES = Region[ {} ];
  ANODES = Region[ {} ];
  CATHODES = Region[ {} ];

  For i In {1:NumWires}
    LWIRE~{i} = Region[ (50+i) ];
    LWIRES += Region[ LWIRE~{i} ];

    If( !Flag_Thin )
      VWIRE~{i} = Region[ (10+i) ];
      VWIRES += Region[ VWIRE~{i} ];
      SWIRE~{i} = Region[ {} ];
      ANODE~{i} = Region[ (20+i) ];
      CATHODE~{i} = Region[ (30+i) ];
    Else
      VWIRE~{i} = Region[ {} ];
      ANODE~{i} = Region[ (60+i) ];
      CATHODE~{i} = Region[ (70+i) ];
      SWIRE~{i} = ElementsOf[ AIR, OnOneSideOf LWIRE~{i} ];
    EndIf

    ANODES += Region[ ANODE~{i} ];
    CATHODES += Region[ CATHODE~{i} ];
  EndFor

  // Abstract regions

  If( !Flag_Thin )
    Vol_C   = Region[ VWIRES ];
    Vol_CC  = Region[ AIR ];
  Else
    Vol_C   = Region[ {LWIRES} ];
    Vol_CC  = Region[ {VWIRES, AIR} ];
  EndIf

  Vol_nu = Region[ {Vol_C, Vol_CC} ];
  Sur_Dirichlet_a = Region[ INF ];
  Electrodes = Region[ {ANODES, CATHODES} ];

  Dom_Hcurl_a = Region[ Vol_nu ];
  Dom_Hgrad_u = Region[ Vol_C ];
  Dom_Hregion_i = Region[ Vol_C ];

  // additional Groups for the semi_analytic approach
  Dom_Hthin_a = ElementsOf[ Vol_nu, OnOneSideOf LWIRES ];
  Vol_Tree = ElementsOf[ Vol_nu, DisjointOf LWIRES ];
  Sur_Tree = Region[ { Sur_Dirichlet_a /*, SKIN*/ } ];
  If( !Flag_SemiAnalytic )
    Lin_Tree = Region[ {} ];
  Else
    Lin_Tree = Region[ { LWIRES } ];
  EndIf
}

Function{
  omega = 2*Pi*Freq;
  mu0 = Pi*4e-7;
  mur = 1.;
  sigma = 5.96e7;

  mu[] = mu0;
  nu[] = 1/mu[];
  sigma[] = sigma;
  skin_depth = Sqrt[ 2/(omega*sigma*mu0*mur) ];

  // Functions for the semi-analytic approach
  i[] = Complex[0,1];
  tau[] = Complex[-1,1]/skin_depth*rw;
  J0[] = JnComplex[0,$1];
  J1[] = JnComplex[1,$1];
  dJ1[] = JnComplex[0,$1]-JnComplex[1,$1]/$1;

  For i In {1:NumWires}
    // Current density
    J[ Region[ {VWIRE~{i}, LWIRE~{i}} ] ] =
      Vector[ 0, 0, WI~{i}/A_c * Exp[ i[]*WP~{i}*deg ] ];

	// local radial coordinate with origin on wire i
    R~{i}[] = Hypot[ X[]-WX~{i} , Y[]-WY~{i} ];
  EndFor

  // shape function of the current, wI[r]
  wI[] = tau[] * J0[tau[]*$1/rw]/J1[tau[]]/(2*A_c);

  //radial analytical solutions with a zero flux condition imposed at $1(=r)=rs
  Analytic_A[] = // per Amp
    (($1>rw) ?  mu0/(2*Pi) * Log[rs/$1] :
				mu0/(2*Pi) * Log[rs/rw] + i[]*( wI[$1]-wI[rw] )/(sigma*omega) );
  AnalyticStatic_A[] = mu0/(2*Pi) * // per Amp
	(($1>rw) ? Log[rs/$1] : Log[rs/rw] + mur*(1-($1/rw)^2)/2 );
  Analytic_B[] = // per Amp
    (($1>=rw) ?  mu0/(2*Pi*$1) :
      mu0*mur/(2*Pi*rw) * J1[tau[]*$1/rw] / J1[tau[]] );

  // Impedance of thin wire p.u. length
  R_DC = 1./(sigma*A_c);
  Analytic_R[] = Re[ wI[rw]/sigma ];
  Analytic_L[] = mu0/(2*Pi) * Log[rs/rw] + Re[ wI[rw]/(i[]*omega*sigma) ];

  If( NumWires == 1 )
    Exact_B[] = Analytic_B[R_1[]] ;
    Correction_B[] = ((R_1[]<rs) ? Analytic_B[R_1[]] : 0 ) ;
  EndIf
  If ( NumWires == 2 )
    Exact_B[] = Analytic_B[R_1[]] + Analytic_B[R_2[]] ;
    Correction_B[] = ((R_1[]<rs) ? Analytic_B[R_1[]] :
                     ((R_2[]<rs) ? Analytic_B[R_2[]] : 0 ));
  EndIf
  If ( NumWires == 3 )
    Exact_B[] = Analytic_B[R_1[]] + Analytic_B[R_2[]] + Analytic_B[R_3[]] ;
    Correction_B[] = ((R_1[]<rs) ? Analytic_B[R_1[]] :
                     ((R_2[]<rs) ? Analytic_B[R_2[]] :
                     ((R_3[]<rs) ? Analytic_B[R_3[]] : 0 )));
  EndIf
}


Jacobian {
  { Name Vol ;
    Case {
      { Region LWIRES ; Jacobian Lin ; Coefficient A_c; }
      { Region All ; Jacobian Vol ; }
    }
  }
}

Integration {
  { Name I1 ;
    Case {
      { Type Gauss ;
        Case {
          { GeoElement Point       ; NumberOfPoints  1 ; }
          { GeoElement Line        ; NumberOfPoints  3 ; }
          { GeoElement Triangle    ; NumberOfPoints  3 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
          { GeoElement Prism       ; NumberOfPoints 21 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
          { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
          { GeoElement Pyramid     ; NumberOfPoints  8 ; }
        }
      }
    }
  }
}

Constraint{
  { Name Impose_U ;
    Case {
      For i In {1:NumWires}
        If( !Flag_Stranded ) // massive
          { Region ANODE~{i} ; Value  0 ; }
        EndIf

        If( Flag_U )
          If( !Flag_Stranded ) // massive
            { Region CATHODE~{i} ; Value -R_DC*Lz*WI~{i} ; }
          Else // stranded
            { Region VWIRE~{i} ; Value -R_DC*Lz*WI~{i} ; }
          EndIf
        EndIf

      EndFor
    }
  }
  { Name Impose_I ;
    Case {
      For i In {1:NumWires}
        If( !Flag_U )
          If( !Flag_Stranded ) // massive
            { Region ANODE~{i}   ; Value  WI~{i} ; }
            { Region CATHODE~{i} ; Value  WI~{i} ; }
          Else // stranded
            { Region VWIRE~{i} ; Value  WI~{i} ; }
            { Region LWIRE~{i} ; Value  WI~{i} ; }
          EndIf
        EndIf
      EndFor
    }
  }

  { Name Hcurl_a_3D; Type Assign;
    Case {
      { Region Region[ Sur_Dirichlet_a ]; Value 0.; }
    }
  }

  { Name GaugeCondition_a; Type Assign;
    Case {
      { Region Vol_Tree; SubRegion Region[ { Sur_Tree, Lin_Tree } ];
        Value 0; }
    }
  }

  // Semi-analytic approach
  { Name Impose_corr ;
    Case {
      For i In {1:NumWires}
        { Region LWIRE~{i} ; Value  1. ; }
      EndFor
    }
  }

  { Name Hcurl_acorr_3D; Type Assign;
    Case {
      { Region Region[ Sur_Dirichlet_a ]; Value 0.; }
      If( Flag_Thin )
        For i In {1:NumWires}
          { Region Region[ LWIRE~{i} ]; Value 1 ; }
        EndFor
      EndIf
    }
  }
}



FunctionSpace {
  // Magnetic vector potential
  { Name Hcurl_a_3D; Type Form1;
    BasisFunction {
      { Name sc; NameOfCoef ac; Function BF_Edge;
		Support Dom_Hcurl_a; Entity EdgesOf[All]; }
    }
    Constraint {
      { NameOfCoef ac;
		EntityType Auto; NameOfConstraint Hcurl_a_3D; }
	  { NameOfCoef ac; EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
	    NameOfConstraint GaugeCondition_a ; }
    }
  }

  // Electric scalar potential for massive conductors
  { Name Hgrad_u_3D; Type Form0;
    BasisFunction {
      { Name sn; NameOfCoef un; Function BF_Node;
		Support Dom_Hgrad_u; Entity NodesOf[ Dom_Hgrad_u, Not Electrodes]; }
      { Name sn2; NameOfCoef un2; Function BF_GroupOfNodes;
		Support Dom_Hgrad_u; Entity GroupsOfNodesOf[ Electrodes ]; }
   }
   GlobalQuantity {
      { Name U; Type AliasOf       ; NameOfCoef un2; }
      { Name I; Type AssociatedWith; NameOfCoef un2; }
    }
    Constraint {
      { NameOfCoef U; EntityType GroupsOfNodesOf; NameOfConstraint Impose_U; }
      { NameOfCoef I; EntityType GroupsOfNodesOf; NameOfConstraint Impose_I; }
    }
  }

  // Electric current per regionl for stranded conductors
  { Name Hregion_i_3D; Type Vector;
    BasisFunction {
      { Name sr; NameOfCoef ir; Function BF_RegionZ;
        Support Dom_Hregion_i; Entity Vol_C; }
    }
    GlobalQuantity {
      { Name Is; Type AliasOf       ; NameOfCoef ir; }
      { Name Us; Type AssociatedWith; NameOfCoef ir; }
    }
    Constraint {
      { NameOfCoef Us; EntityType Region; NameOfConstraint Impose_U; }
      { NameOfCoef Is; EntityType Region; NameOfConstraint Impose_I; }
    }
  }

  // Function spaces for the semi-analytical approach

  // { Name Hcurl_acorr_3D; Type Form1;
  //   BasisFunction {
  //     { Name sw; NameOfCoef aw; Function BF_Edge;
  //       Support Dom_Hthin_a; Entity EdgesOf[ Vol_nu, ConnectedTo LWIRES ]; }
  //   }
  //   Constraint {
  //     { NameOfCoef aw;
  //   	EntityType Auto; NameOfConstraint Hcurl_a_3D; }
  //     { NameOfCoef aw; EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
  //     	NameOfConstraint GaugeCondition_a ; }
  //   }
  // }

  { Name Hcurl_athin_3D; Type Form1;
    BasisFunction {
      { Name si; NameOfCoef ai; Function BF_GroupOfEdges;
        Support Dom_Hcurl_a; Entity GroupsOfEdgesOf[ LWIRES ]; }
      { Name sj; NameOfCoef aj; Function BF_Edge;
        Support Dom_Hcurl_a; Entity EdgesOf[ Vol_nu, Not LWIRES ]; }
    }
    GlobalQuantity {
      { Name F; Type AliasOf       ; NameOfCoef ai; }
      { Name I; Type AssociatedWith; NameOfCoef ai; }
    }
    Constraint {
      { NameOfCoef I; EntityType Region; NameOfConstraint Impose_corr; }
      { NameOfCoef aj; EntityType Auto; NameOfConstraint Hcurl_a_3D; }
      { NameOfCoef aj; EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
       	NameOfConstraint GaugeCondition_a ; }
    }
  }
  { Name Hcurl_asleeve_3D; Type Form1;
    BasisFunction {
      { Name si; NameOfCoef ai; Function BF_GroupOfEdges;
        Support Dom_Hthin_a; Entity GroupsOfEdgesOf[ LWIRES ]; }
      { Name sj; NameOfCoef aj; Function BF_Edge;
        Support Dom_Hthin_a; Entity EdgesOf[ Vol_nu, ConnectedTo LWIRES ]; }
    }
    GlobalQuantity {
      { Name F; Type AliasOf       ; NameOfCoef ai; }
      { Name I; Type AssociatedWith; NameOfCoef ai; }
    }
    Constraint {
      { NameOfCoef I; EntityType Region; NameOfConstraint Impose_corr; }
      { NameOfCoef aj; EntityType Auto; NameOfConstraint Hcurl_a_3D; }
    }
  }
}

Formulation {
  { Name MagnetoDynamics; Type FemEquation;

    If( !Flag_SemiAnalytic )
      // Conventional massive and stranded conductor formulation
      // If Flag_Thin is true, naive thine wire formulations

      If( !Flag_Stranded ) // Massive conductor
        Quantity {
          { Name a;  Type Local;  NameOfSpace Hcurl_a_3D; }

          { Name v;  Type Local;  NameOfSpace Hgrad_u_3D; }
          { Name U;  Type Global; NameOfSpace Hgrad_u_3D [U]; }
          { Name I;  Type Global; NameOfSpace Hgrad_u_3D [I]; }
        }

        Equation {
          Integral { [ nu[] * Dof{d a} , {d a} ];
            In Vol_nu; Jacobian Vol; Integration I1; }

          Integral { DtDof[ sigma[] * Dof{a} , {a} ];
            In Vol_C; Jacobian Vol; Integration I1; }
          Integral { [ sigma[] * Dof{d v} , {a} ];
            In Vol_C; Jacobian Vol; Integration I1; }
          Integral { DtDof[ sigma[] * Dof{a} , {d v} ];
            In Vol_C; Jacobian Vol; Integration I1; }
          Integral { [ sigma[] * Dof{d v} , {d v} ];
            In Vol_C; Jacobian Vol; Integration I1; }

          GlobalTerm { [Dof{I} , {U} ]; In CATHODES; }
        }

        Else // stranded conductor
        Quantity {
          { Name a;  Type Local;  NameOfSpace Hcurl_a_3D; }

          { Name  i; Type Local;  NameOfSpace Hregion_i_3D; }
          { Name Is; Type Global; NameOfSpace Hregion_i_3D [Is]; }
          { Name Us; Type Global; NameOfSpace Hregion_i_3D [Us]; }
        }

        Equation {
          Integral { [ nu[] * Dof{d a} , {d a} ];
            In Vol_nu; Jacobian Vol; Integration I1; }
          Integral { [ -Dof{i}/A_c , {a} ];
            In Vol_C; Jacobian Vol; Integration I1; }

          // Integral { [ -J[] , {a} ];
          //   In Vol_C; Jacobian Vol; Integration I1; }

          // Us is the voltage drop
          // grad v = -Dt a - j/sigma
          Integral { DtDof [ Dof{a} , {i} ];
            In Vol_C; Jacobian Vol; Integration I1;}
          Integral { [ Dof{i}/(sigma[]*A_c) , {i} ];
            In Vol_C; Jacobian Vol; Integration I1;}
          GlobalTerm { [ Dof{Us}*A_c , {Is} ]; In Vol_C; }
        }
      EndIf

    EndIf

    If( Flag_SemiAnalytic )
        // Semi-analytic correction methods

      Quantity {
        { Name a;  Type Local; NameOfSpace Hcurl_athin_3D; }
        { Name F; Type Global; NameOfSpace Hcurl_athin_3D [F]; }
        { Name I; Type Global; NameOfSpace Hcurl_athin_3D [I]; }

        { Name as; Type Local;  NameOfSpace Hcurl_asleeve_3D; }
        { Name Fs; Type Global; NameOfSpace Hcurl_asleeve_3D [F]; }
        { Name Is; Type Global; NameOfSpace Hcurl_asleeve_3D [I]; }
      }

      Equation {

        Integral { [ nu[] * Dof{d a} , {d a} ];
          In Vol_nu; Jacobian Vol; Integration I1; }
        GlobalTerm { [ -Dof{I}*NbDivision , {F} ];
          In LWIRES ; }

        Integral { [ nu[] * Dof{d as} , {d as} ];
          In Vol_nu; Jacobian Vol; Integration I1; }
        GlobalTerm { [ -Dof{Is}*NbDivision , {Fs} ];
          In LWIRES ; }

        // Integral { [ -Dof{i}/A_c , {as} ];
        //   In Vol_C; Jacobian Vol; Integration I1; }

        /*
              GlobalTerm { [ Analytic_R[] * Dof{Is} , {Is} ];
              In Vol_C;}
              Integral { DtDof [ Dof{a}/A_c , {i} ];
              In Vol_C; Jacobian Vol; Integration I1; }
              Integral { DtDof [ -Dof{as}/A_c , {i} ];
              In Vol_C; Jacobian Vol; Integration I1;}
              GlobalTerm { DtDof [ Analytic_L[] * Dof{Is} , {Is} ];
              In Vol_C;}
              GlobalTerm { [ Dof{Us} , {Is} ];
              In Vol_C; }
        */
      }
    EndIf
  }
}


Resolution {
 { Name Dynamic;
   System {
     { Name S; NameOfFormulation MagnetoDynamics;
       Type ComplexValue; Frequency Freq; }
   }
   Operation {
     CreateDir[DataDir];

      Generate[S]; Solve[S]; SaveSolution[S];

	  If( Flag_Maps == 1 )
		// DeleteFile["U.dat"];
		// DeleteFile["I.dat"];
		// DeleteFile["Z.dat"];

        PostOperation[map];
	  EndIf
      PostOperation[integaz];
      PostOperation[cut];

      For i In {1:NumWires}
        Print[ {i, rw, rs, A_c, skin_depth, R_DC, WI~{i}/A_c, mu0*WI~{i}/(2*Pi*rw)},
          Format "WIRE %2g: rw = %7.3e rs = %7.3e A_c = %7.3e delta = %7.3e R_DC = %7.3e J = %7.3e Bmax = %7.3e"] ;
        // Print[ {i, rw, Sqrt[ $sarea~{i}/Pi], rs, $sarea~{i}, Pi*rs^2, skin_depth, $intby~{i}/$sarea~{i}},
        //   Format "WIRE %2g: rw = %7.3e rs = %7.3e (%7.3e) as = %7.3e (%7.3e) delta = %7.3e bave = %7.3e" ] ;
      EndFor
    }
  }
}

PostProcessing {
  { Name MagnetoDynamics; NameOfFormulation MagnetoDynamics;
    PostQuantity {
      { Name a;
        Value { Local { [ {a}]; In  Dom_Hcurl_a; Jacobian Vol; }}}
      { Name b;
        Value { Local { [ {d a}]; In  Dom_Hcurl_a; Jacobian Vol; }}}
      { Name by;
        Value { Local { [ Cart2Pol[ Norm[ {d a}]] ];
            In  Dom_Hcurl_a; Jacobian Vol; }}}

      If( Flag_SemiAnalytic )
        { Name bs;
          Value { Local { [ {d as} ]; In Vol_nu; Jacobian Vol; }}}
        { Name bsy;
          Value { Local { [ Norm[ {d as} ] ]; In Vol_nu; Jacobian Vol; }}}
        { Name bbsy;
          Value { Local { [ Norm[ {d a} - {d as} ] ]; In Vol_nu; Jacobian Vol; }}}
        { Name bcorry;
          Value { Local { [ Norm[ {d a} - {d as} ] + Correction_B[] ];
              In Vol_nu; Jacobian Vol; }}}
      EndIf



      If( !Flag_SemiAnalytic )
        If( !Flag_Stranded ) // massive
          { Name J;
            Value { Local { [ -sigma[] * ( Dt[{a}] + {d v} ) ];
                In Vol_C; Jacobian Vol; }}}
          { Name U;
            Value { Term { [ {U} ]; In Electrodes; }}}
          { Name I;
            Value { Term { [ {I} ]; In Electrodes; }}}
          { Name R;
            Value { Term { [ Re[ -{U}/{I}/Lz ] ]; In Electrodes; }}}
          { Name L; // actually total flux/I
            Value { Term { [ Im[ -{U}/{I}/omega/Lz ] ]; In Electrodes; }}}
        Else // stranded
          { Name J;
            Value { Local { [ {Is}/A_c ];
                In Vol_C; Jacobian Vol; }}}
          { Name U;
            Value { Term { [ {Us} ]; In Vol_C; }}}
          { Name I;
            Value { Term { [ {Is} ]; In Vol_C; }}}
          { Name R;
            Value { Term { [ Re[ -{Us}/{Is}/Lz ] ]; In Vol_C; }}}
          { Name L; // actually total flux/I
            Value { Term { [ Im[ -{Us}/{Is}/omega/Lz ] ]; In Vol_C; }}}
        EndIf
      EndIf

      If( Flag_SemiAnalytic )
        { Name F;
          Value { Term { [ {F} ]; In Vol_C; }}}
        { Name I;
          Value { Term { [ {I} ]; In Vol_C; }}}
        { Name J;
          Value { Local { [ {I}/A_c ];
              In Vol_C; Jacobian Vol; }}}
        { Name U;
          Value { Term { [ i[]*omega*{F}*10 + Analytic_R[]*{I} ]; In Vol_C; }}}
        { Name R;
          Value { Term { [ Analytic_R[] + {I}*0 ]; In Vol_C; }}}
        { Name L; // actually total flux/I
          Value { Term { [ {F}/{I}/Lz ]; In Vol_C; }}}
      EndIf

    }
  }
}

PostOperation map UsingPost MagnetoDynamics {

  Print[ b, OnElementsOf Vol_Tree, File "b.pos"];

  If( !Flag_SemiAnalytic )
    Print[ J, OnElementsOf Vol_C, File "j.pos"];
  Else
    Print[ bs, OnElementsOf Vol_nu, File "bs.pos"];
    // PrintGroup[ EdgesOf[ Vol_nu, ConnectedTo LWIRES ],
    //   In Vol_nu, File "group.pos"];
    //PrintGroup[ _CO_Entity_71, In Vol_nu, File "Tree.pos"];
  EndIf
  //PrintGroup[ _CO_Entity_30, In Vol_nu, File "Tree.pos"];
    PrintGroup[ EdgesOfTreeIn[ Vol_Tree , StartingOn { Sur_Tree, Lin_Tree } ],
      In Vol_nu, File "Tree.pos"];
}

PostOperation integaz UsingPost MagnetoDynamics {
  For i In {1:NumWires}
    If( !Flag_SemiAnalytic )
      If( !Flag_Stranded ) // massive
        Print[ U, OnRegion CATHODES, File > "U.dat", Format Table,
          SendToServer Sprintf["Results/1Voltage/Wire %g",i]];
        Print[ I, OnRegion CATHODES, File > "I.dat", Format Table,
          SendToServer Sprintf["Results/2Current/Wire %g",i]];
        Print[ R, OnRegion CATHODES, File > "Z.dat", Format Table,
          SendToServer Sprintf["Results/3Resistance p.u.l./Wire %g",i]];
        Print[ L, OnRegion CATHODES, File > "Z.dat", Format Table,
          SendToServer Sprintf["Results/4Inductance p.u.l./Wire %g",i]];
      Else
        Print[ U, OnRegion Vol_C, File > "U.dat", Format Table,
          SendToServer Sprintf["Results/1Voltage/Wire %g",i]];
        Print[ I, OnRegion Vol_C, File > "I.dat", Format Table,
          SendToServer Sprintf["Results/2Current/Wire %g",i]];
        Print[ R, OnRegion Vol_C, File > "Z.dat", Format Table,
          SendToServer Sprintf["Results/3Resistance p.u.l./Wire %g",i]];
        Print[ L, OnRegion Vol_C, File > "Z.dat", Format Table,
          SendToServer Sprintf["Results/4Inductance p.u.l./Wire %g",i]];
      EndIf
    EndIf
    If( Flag_SemiAnalytic )
      Print[ F, OnRegion Vol_C, File > "Flux.dat", Format Table,
        SendToServer Sprintf["Results/0Flux/Wire %g",i]];
      Print[ U, OnRegion Vol_C, File > "U.dat", Format Table,
        SendToServer Sprintf["Results/1Voltage/Wire %g",i]];
      Print[ I, OnRegion Vol_C, File > "I.dat", Format Table,
        SendToServer Sprintf["Results/2Current/Wire %g",i]];
      Print[ R, OnRegion Vol_C, File > "Z.dat", Format Table,
        SendToServer Sprintf["Results/3Resistance p.u.l./Wire %g",i]];
      Print[ L, OnRegion Vol_C, File > "Z.dat", Format Table,
        SendToServer Sprintf["Results/4Inductance p.u.l./Wire %g",i]];
    EndIf
  EndFor
}

NbPoints = 1000;
Xcut = 0.1*box;
Zcut = Lz/2;

PostOperation cut UsingPost MagnetoDynamics {
  Print [ by, OnLine { {-Xcut,0,0} {Xcut,0,Zcut} }{NbPoints},
          Format Gmsh, File "by.pos" ];
  Echo[ StrCat["l=PostProcessing.NbViews-1;",
      "View[l].Name = 'cut by';",
      "View[l].Axes = 3;",
      "View[l].LineWidth = 3;",
      "View[l].Type = 2;"],
    File "tmp.geo", LastTimeStepOnly];

  If( Flag_SemiAnalytic )
    Print [ bsy, OnLine { {-Xcut,0,0} {Xcut,0,Zcut} }{NbPoints},
      Format Gmsh, File "bcorry.pos" ];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].Name = 'cut bsy ';",
        "View[l].Axes = 3;",
        "View[l].LineWidth = 3;",
        "View[l].Type = 2;"],
      File "tmp.geo", LastTimeStepOnly];

    Print [ bbsy, OnLine { {-Xcut,0,0} {Xcut,0,Zcut} }{NbPoints},
      Format Gmsh, File "bcorry.pos" ];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].Name = 'cut by-bsy ';",
        "View[l].Axes = 3;",
        "View[l].LineWidth = 3;",
        "View[l].Type = 2;"],
      File "tmp.geo", LastTimeStepOnly];

    Print [ bcorry, OnLine { {-Xcut,0,0} {Xcut,0,Zcut} }{NbPoints},
      Format Gmsh, File "bcorry.pos" ];
    Echo[ StrCat["l=PostProcessing.NbViews-1;",
        "View[l].Name = 'cut by corrected';",
        "View[l].Axes = 3;",
        "View[l].LineWidth = 3;",
        "View[l].Type = 2;"],
      File "tmp.geo", LastTimeStepOnly];
  EndIf

  // cuts in txt format for Gnuplot
  // Print [ exact, OnLine { {0,0,0} {Xcut,0,0} } {NbPoints},
  //         Format SimpleTable, File "Cut_analytic.txt" ];


  If( !Flag_SemiAnalytic )
    Print [ by, OnLine { {0,0,0} {Xcut,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_byref.txt" ];
  Else
    Print [ by, OnLine { {0,0,0} {Xcut,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_by.txt" ];
    Print [ bsy, OnLine { {0,0,0} {Xcut,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_bsy.txt" ];
    Print [ bbsy, OnLine { {0,0,0} {Xcut,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_bbsy.txt" ];
    Print [ bcorry, OnLine { {0,0,0} {Xcut,0,0} } {NbPoints},
      Format SimpleTable, File "Cut_bcorry.txt" ];
  EndIf
}
