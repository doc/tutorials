L = 20e-3;
l = 1e-3;
N1 = 5;
N2 = 50;
Point(1) = {0,0,0};
l1() = Extrude{0,l,0}{ Point{1}; Layers{N1}; };
l2() = Extrude{0,0,l}{ Curve{l1(1)}; Layers{N1}; Recombine; };
l3() = Extrude{L,0,0}{ Surface{l2(1)}; Layers{N2}; Recombine; };

Physical Volume("Beam", 100) = {l3(1)};
Physical Surface("Clamp", 1) = {l2(1)};
Physical Surface("Top", 2) = {l3(3)};
Physical Surface("Right", 3) = {l3(0)};
