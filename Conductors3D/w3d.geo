Include "w3d_common.pro";

// global Gmsh options
Mesh.Optimize = 1; // optimize quality of tetrahedra
Mesh.VolumeEdges = 0; // hide volume edges
Geometry.ExactExtrusion = 0; // to allow rotation of extruded shapes
Solver.AutoMesh = 2; // always remesh if necessary (don't reuse mesh on disk)

lc1 = (Flag_Thin) ? MeshSizeThinWire*mm : MeshSizeWire*mm ;
lc2 = box/10; // mesh size at outer surface

llWires[] = {};
centerWires[] = {};
surfWires[] = {};

If( !Flag_Thin || (Flag_Thin && Flag_RegSleeve)) 
  // then circles are in the geometry
  // their radius is R=rw if Flag_Thin != 0
  // or R=rs in the "regular sleeve case" (Flag_Corr == 2)
  
  R = ( Flag_Thin ) ? rs : rw;
      
  For i In {1:NumWires}
    pC = newp; Point(pC) = {WX~{i}  , WY~{i}       , 0, lc1};
    p1 = newp; Point(p1) = {WX~{i}+R, WY~{i}       , 0, lc1};
    p2 = newp; Point(p2) = {WX~{i}  , WY~{i}+R, 0, lc1};
    p3 = newp; Point(p3) = {WX~{i}-R, WY~{i}       , 0, lc1};
    p4 = newp; Point(p4) = {WX~{i}  , WY~{i}-R, 0, lc1};
    c1 = newl; Circle(c1) = {p1,pC,p2}; 
    c2 = newl; Circle(c2) = {p2,pC,p3};
    c3 = newl; Circle(c3) = {p3,pC,p4}; 
    c4 = newl; Circle(c4) = {p4,pC,p1};
    l1 = newl; Line(l1) = {pC,p1};
    l2 = newl; Line(l2) = {pC,p2};
    l3 = newl; Line(l3) = {pC,p3};
    l4 = newl; Line(l4) = {pC,p4};
    ll1 = newll; Line Loop(ll1) = {l1,c1,-l2};
    s1 = news; Plane Surface(s1) = {ll1};
    ll2 = newll; Line Loop(ll2) = {l2,c2,-l3};
    s2 = news; Plane Surface(s2) = {ll2};
    ll3 = newll; Line Loop(ll3) = {l3,c3,-l4};
    s3 = news; Plane Surface(s3) = {ll3};
    ll4 = newll; Line Loop(ll4) = {l4,c4,-l1};
    s4 = news; Plane Surface(s4) = {ll4};

    ll5 = newll;
    Line Loop(ll5) = {c1,c2,c3,c4};
    llWires[] += { ll5 };
    surfWires~{i-1}[] = {s1,s2,s3,s4};
    surfWires[] += surfWires~{i-1}[];
    centerWires[] += {pC};
  EndFor
Else
  // then geometry only contains line conductors
  For i In {1:NumWires}
    pC = newp; Point(pC) = {WX~{i}, WY~{i}, 0, lc1};
    centerWires[] += {pC};
  EndFor

EndIf

// create air box around wires
BoundingBox; // recompute model bounding box
cx = (General.MinX + General.MaxX) / 2;
cy = (General.MinY + General.MaxY) / 2;
cz = (General.MinZ + General.MaxZ) / 2;

// lx = 2*inf + General.MaxX - General.MinX;
// ly = 2*inf + General.MaxY - General.MinZ;
lx = 2*box;
ly = 2*box;

p1 = newp; Point (p1) = {cx-lx/2, cy-ly/2, 0, lc2};
p2 = newp; Point (p2) = {cx+lx/2, cy-ly/2, 0, lc2};
p3 = newp; Point (p3) = {cx+lx/2, cy+ly/2, 0, lc2};
p4 = newp; Point (p4) = {cx-lx/2, cy+ly/2, 0, lc2};
l1 = newl; Line(l1) = {p1, p2};
l2 = newl; Line(l2) = {p2, p3};
l3 = newl; Line(l3) = {p3, p4};
l4 = newl; Line(l4) = {p4, p1};
ll1 = newll; Line Loop(ll1) = {l1,l2,l3,l4};

Skin[] = {};
  
If( !Flag_Thin ) // Wires with finite radius

  For i In {1:NumWires}
    e[] = Extrude {0,0,Lz} 
          { Surface{ surfWires~{i-1}[] } ; /*Layers{1}; Recombine;*/ } ;
    Physical Volume (Sprintf("VWIRE_%g",i), 10+i) = { e[ {1,6,11,16} ] };
    Physical Surface(Sprintf("SANODE_%g",i), 20+i) = { surfWires~{i-1}[] };
    Physical Surface(Sprintf("SCATHODE_%g",i), 30+i) = { e[ {0,5,10,15} ] };
    Skin[] += e[ {3,8,13,18} ];
  EndFor
  s1 = news; Plane Surface(s1) = {ll1,-llWires[]};
  e[] = Extrude {0,0,Lz} { Surface{ s1 } ; /*Layers{1}; Recombine;*/ } ;
  Physical Volume ("AIR", 1) = { e[1] };

ElseIf( Flag_RegSleeve ) // regular sleeve
  
  Wires[]=  {};
  For i In {1:NumWires}
    e[] = Extrude {0,0,Lz} { Point{ centerWires[i-1] } ; /*Layers{1};*/ } ;
    Transfinite Curve{ e[1] } = NbDivision+1;
    Physical Line  (Sprintf("LWIRE_%g",i), 50+i) = { e[1] };
    Physical Point (Sprintf("PANODE_%g",i), 60+i) = { centerWires[i-1] };
    Physical Point (Sprintf("PCATHODE_%g",i), 70+i) = { e[0] };

    e[] = Extrude {0,0,Lz} { Surface{ surfWires~{i-1}[] } ; 
      /*Layers{1}; Recombine;*/ } ;
    Wires() += e[ {1,6,11,16} ];
    Physical Volume (Sprintf("VWIRE_%g",i), 10+i) = { e[ {1,6,11,16} ] };
    Physical Surface(Sprintf("SANODE_%g",i), 20+i) = { surfWires~{i-1}[] };
    Physical Surface(Sprintf("SCATHODE_%g",i), 30+i) = { e[ {0,5,10,15} ] };  
  EndFor
  s1 = news; Plane Surface(s1) = {ll1,-llWires[]};
  e[] = Extrude {0,0,Lz} { Surface{ s1 } ; /*Layers{1}; Recombine;*/ } ;
  Physical Volume ("AIR", 1) = { e[1], Wires[] };
  // auxiliary Physical Volume to test the tree.
  // Note that two Physical Volumes defined on a same region
  // yield duplicate finite element in GetDP. 
  //Physical Volume ("BLA", 5) = { e[1] }; this 

Else

  s1 = news; Plane Surface(s1) = {ll1};
  e[] = Extrude {0,0,Lz} { Surface{ s1 } ; /*Layers{1}; Recombine;*/ } ;
  s2 = e[0];
  v1 = e[1];
  Physical Volume ("AIR", 1) = { v1 };
  For i In {1:NumWires}
    e[] = Extrude {0,0,Lz} { Point{ centerWires[i-1] } ; /*Layers{1};*/ } ;
    Transfinite Curve{ e[1] } = NbDivision+1;
    //Printf("e=", e[]);
    Physical Line  (Sprintf("LWIRE_%g",i), 50+i) = { e[1] };
    Physical Point (Sprintf("PANODE_%g",i), 60+i) = { centerWires[i-1] };
    Physical Point (Sprintf("PCATHODE_%g",i), 70+i) = { e[0] };
    // embed line conductors in mesh
    Point { centerWires[i-1] } In Surface { s1 };
    Point { e[0] } In Surface { s2 };
    Curve { e[1] } In Volume { v1 };
  EndFor
EndIf 

Physical Surface("INF", 2) = { CombinedBoundary{ Volume{ : }; } };
Physical Surface("SKIN", 3) = Skin[];

Electrodes[] = {};
If( !Flag_Thin ) // Wires with finite radius
  For i In {1:NumWires}
    Electrodes[] += {20+i, 30+i};
  EndFor
EndIf  

Physical Line("LINTREE", 4) = { CombinedBoundary { Physical Surface { Electrodes[] }; } };
