/*
  Tutorial: 3D Magnetostatics with rotation and periodic boundary conditions

  Features:
  - OpenCascade solid modelling
  - geometrical identification of faces
  - antiperiodic boundary conditions
  - sliding surface to account for rotor rotation

  To compute the solution interactively from the Gmsh GUI:
      File > Open > microstrip.pro
      Run (button at the bottom of the left panel)
*/

Include "rfpm_common.pro";

DefineConstant[
  R_ = {"Static", Choices {"Static", "QuasiStatic"}, Visible 1,
    Name "GetDP/1ResolutionChoices"},
  C_ = {"-solve -v 4 -pos -v2", Visible 0,
    Name "GetDP/9ComputeCommand"},
  P_ = { "", Choices {"Fields", "Check_Periodicity"}, Visible 0,
    Name "GetDP/2PostOperationChoices"}
];

Flag_QuasiStatic = !StrCmp[ R_, "QuasiStatic"];

RotorPosition = deg * DefineNumber[0, Name "Options/Rotor position [deg]",
  Visible !Flag_QuasiStatic];
AngularStep = deg * DefineNumber[2, Name "Options/1Angular step [deg]",
  Visible Flag_QuasiStatic];
NumStep = DefineNumber[20, Name "Options/1Number of steps",
  Visible Flag_QuasiStatic];
Gauge = DefineNumber[1, Name "Options/Gauging", 
					 Choices{ 0="MUMPS", 1="Tree"}];

rpm = 2. * Pi / 60. ;
w1 = 1000 * rpm ;
Mag_DTime = AngularStep / Fabs[w1] ;
Mag_TimeMax = Mag_DTime * NumStep;

Group {
  // Geometrical regions
  Rotor     = Region[1];
  Magnet    = Region[2];
  Stator    = Region[3];
  AirRotor  = Region[4];
  AirStator = Region[5];
  AirRing   = Region[6];

  Outer               = Region[10];
  Top                 = Region[11];
  Bottom              = Region[12];
  Sur_RotorPerMaster  = Region[13];
  Sur_RotorPerSlave   = Region[14];
  Sur_StatorPerMaster = Region[15];
  Sur_StatorPerSlave  = Region[16];
  Sur_SlidingMaster   = Region[17];
  Sur_SlidingSlave    = Region[18];

  Lin_SlidingSubmaster = Region[20];
  Lin_SlidingSubslave  = Region[21];
  Lin_TreeLines        = Region[22];

  // Abstract regions
  Iron = Region[ { Rotor, Stator } ];
  Air = Region[ { AirRotor, AirStator, AirRing } ];

  Vol_Conductors  = Region[ {} ] ;
  Vol_Magnets     = Region[ Magnet ];
  Vol_Mag         = Region[ { Air, Magnet, Iron } ];
  Vol_Moving      = Region[ { Rotor, AirRotor, Magnet } ] ;

  Sur_Dirichlet = Region[ { Outer, Top, Bottom } ];

  Sur_Link = Region[ {Sur_SlidingMaster, Sur_SlidingSlave,
                      Sur_StatorPerMaster, Sur_StatorPerSlave,
                      Sur_RotorPerMaster, Sur_RotorPerSlave} ] ;
  Dom_Hcurl_a = Region[ { Vol_Mag, Sur_Link } ]; // No Neumann surface

  Sur_Tree_Sliding = Region[ { Sur_SlidingMaster, Sur_SlidingSlave } ];
  Lin_Tree = Region[ Lin_TreeLines ];
  Sur_Tree = Region[ { Sur_Link, Sur_Dirichlet } ];
  Vol_Tree = Region[ Vol_Mag ];
}

Function{
  mu0 = Pi*4e-7;
  br = 1.2;
  mur = 3;

  nu[ Iron ] = 1/(mur*mu0);
  nu[ Air ] = 1/mu0;
  nu[ Magnet ] = 1/mu0;
  br[] = br*Unit[ Vector[X[], Y[], 0] ];

  js[] = 0;
}

Function {
  // Functions for the Sliding surface technique:
  // - a1 is the angular span (in radian) of the sliding regions SlidingMaster 
  //   and SlidingSlave.
  // - a0 is the mesh step of the sliding region in the direction of rotation.
  // - a2[] is the angular position (in radian) of the rotor relative to its 
  //   reference position (that in the msh file) assumed to be aligned with the
  //   stator.
  // - a3[] is the shear angle of the AirRing region to adapt to a2[] values 
  //   that are not multiple of a0. On has fabs(a3[]) < a0/2.
  // - AlignWithMaster[] maps a point of coordinates {X[], Y[], Z[]} onto its
  //   image by periodicity in the open set SlidingMaster - SlidingSubMaster.
  // - Coef[] = -1 or 1 is the coefficient of (anti-)periodicity condition.
  //   This function is evaluated on SlidingMaster nodes
  // - fFloor[] is a robust version of Floor[] for float-represented integer 
  //   variables.
  // - fRem[a,b] substracts from a multiple of b so that the result is in [0,b[
  //   It is a robust version of the remainder of the integer division for 
  //   float-represented integer variables.

  Periodicity = -1. ;  // -1 for antiperiodicity, 1 for periodicity
  RotatePZ[] = Rotate[ XYZ[], 0, 0, $1 ] ;
  Tol = 1e-8 ; fFloor[] = Floor[ $1 + Tol ] ; fRem[] = $1 - fFloor[ $1 / $2 ] * $2;
  deg = Pi/180;
  a1 = (ModelAngleMax-ModelAngleMin)*deg ;
  a0 = a1/NbrDiv ; // angular span of one moving band element
  a2[] = $RotorPosition ;
  a3[] = ( ( fRem[ a2[], a0 ]#2 <= 0.5*a0 ) ? #2 : #2-a0 ) ;
  AlignWithMaster[] = RotatePZ[ - fFloor[ (Atan2[ Y[], X[] ] - a3[] )/a1 ] * a1 ] ;
  RestoreRef[] = XYZ[] - Vector[ 0, 0, SlidingSurfaceShift ] ;
  Coef[] = ( fFloor[ ((Atan2[ Y[], X[] ] - a2[] )/a1) ] % 2 ) ? Periodicity : 1. ;
}

Group {
  DefineGroup[ DomainInf ];
  DefineVariable[ Val_Rint, Val_Rext ];
}

Jacobian {
  { Name Vol ;
    Case { { Region DomainInf ; Jacobian VolSphShell {Val_Rint, Val_Rext} ; }
           { Region All ;       Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case { { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int ;
    Case {
      { Type Gauss ;
	Case {
	  { GeoElement Line        ; NumberOfPoints  4 ; }
	  { GeoElement Triangle    ; NumberOfPoints  4 ; }
	  { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
	  { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
	  { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
	  { GeoElement Prism       ; NumberOfPoints  21 ; }
	}
      }
    }
  }
}

Constraint {
  { Name a ;
    Case {
      { Region Sur_Dirichlet ; Value 0. ; }

      // Periodicity condition on lateral faces
      { Type Link ; Region Sur_StatorPerSlave ; RegionRef Sur_StatorPerMaster ;
      	ToleranceFactor 1e-8;
      	Coefficient Periodicity ; Function RotatePZ[ -a1 ] ; }
      { Type Link ; Region Sur_RotorPerSlave ; RegionRef Sur_RotorPerMaster ;
      	ToleranceFactor 1e-8;
      	Coefficient Periodicity ; Function RotatePZ[ -a1 ] ; }

      // Sliding surface
      { Type Link ; Region Sur_SlidingSlave ; SubRegion Lin_SlidingSubslave ;
      	RegionRef Sur_SlidingMaster ; SubRegionRef Lin_SlidingSubmaster ;
      	ToleranceFactor 1e-8;
      	Coefficient Coef[] ; Function AlignWithMaster[] ;
		FunctionRef RestoreRef[] ;
      }
    }
  }
  // A correct spanning-tree is essential to the validity of the model.
  // The spanning-tree must be autosimilar by rotation on the sliding surfaces
  // (SubRegion2 clause, only the edges aligned with the Z axis are placed in 
  // the tree), and be also a spanning-tree and Dirichlet and Link surfaces and
  // their boundaries (SubRegion clause).
  { Name GaugeCondition_a ; Type Assign ;
    Case {
      { Region Vol_Tree ; Value 0. ;
      	SubRegion Region[ { Sur_Tree, Lin_Tree} ] ;
        SubRegion2 Region[ Sur_Tree_Sliding, AlignedWith Z ] ;
      }
    }
  }
}

FunctionSpace {
  { Name Hcurl_a; Type Form1;
    BasisFunction {
      { Name se;  NameOfCoef ae;  Function BF_Edge;
	Support Dom_Hcurl_a ; Entity EdgesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef ae;  EntityType EdgesOf ; NameOfConstraint a; }
      If(Gauge == 1) // spanning tree gauging
        { NameOfCoef ae;  EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
          NameOfConstraint GaugeCondition_a ; }
      EndIf
    }
  }
}

Formulation {
  { Name MagSta_a; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ;
        In Vol_Mag ; Jacobian Vol ; Integration Int ; }
      Galerkin { [ -nu[] * br[] , {d a} ] ;
        In Vol_Magnets ; Jacobian Vol ; Integration Int ; }
      Galerkin { [ -js[] , {a} ] ; In Vol_Conductors ;
                 Jacobian Vol ; Integration Int ; }
    }
  }
}

Resolution {
  { Name Static ;
    System {
      { Name Sys_Mag ; NameOfFormulation MagSta_a ;}
    }
    Operation {
      Evaluate[$RotorPosition = RotorPosition];
      ChangeOfCoordinates [ NodesOf[ Vol_Moving ], RotatePZ[ a2[] ] ] ;
      ChangeOfCoordinates [ NodesOf[ Sur_SlidingMaster ], RotatePZ[ a3[] ] ] ;
      Evaluate[ $a2 = a2[] ];
      Evaluate[ $a3 = a3[] ];
      Evaluate[ $aa = Fmod[ a2[], a0 ] ];
      Evaluate[ $bb = fRem[ a2[], a0 ] ];
      Print[ {$RotorPosition/deg, $a2/deg, $a3/deg, $aa/deg, $bb/deg},
	     Format "wt=%e a2=%e a3=%e %e %e"] ;

      UpdateConstraint[Sys_Mag] ;
      Generate Sys_Mag ; Solve Sys_Mag ; SaveSolution Sys_Mag ;
      PostOperation [Check_Periodicity];
      PostOperation [Fields];
    }
  }

  { Name QuasiStatic ;
    System {
      { Name Sys_Mag ; NameOfFormulation MagSta_a ; }
    }
    Operation {
      Evaluate[$RotorPosition = RotorPosition];
      Generate Sys_Mag ; Solve Sys_Mag ; SaveSolution Sys_Mag ;
      PostOperation [Fields];
      TimeLoopTheta{
	Time0 0 ; TimeMax Mag_TimeMax ; DTime Mag_DTime ; Theta 1. ;
	Operation {
	  Evaluate[$RotorPosition = RotorPosition + w1*$Time];
	  ChangeOfCoordinates[NodesOf[ Vol_Moving ], RotatePZ[ w1*$DTime ] ] ;
	  ChangeOfCoordinates[NodesOf[ Sur_SlidingMaster ], RotatePZ[ a3[] ] ] ;
	  UpdateConstraint[Sys_Mag] ;
	  Generate Sys_Mag ; Solve Sys_Mag ; SaveSolution Sys_Mag ;
	  PostOperation [Fields];
	  ChangeOfCoordinates [ NodesOf[ Sur_SlidingMaster ], RotatePZ[ -a3[] ] ] ;
	}
      }
    }
  }
}

PostProcessing {
  { Name MagSta_a ; NameOfFormulation MagSta_a ;
    PostQuantity {
      { Name b ; Value { Local { [ {d a} ];
            In Vol_Mag ; Jacobian Vol; } } }
      { Name bsurf ; Value { Local { [ {d a} ];
            In Sur_Link ; Jacobian Sur; } } }
      { Name asurf ; Value { Local { [ {a} ];
	    In Sur_Link ; Jacobian Sur; } } }
      { Name br ; Value { Local { [ br[] ];
	    In Vol_Magnets ; Jacobian Vol; } } }
      { Name a ; Value { Local { [ {a} ];
	    In Vol_Mag ; Jacobian Vol; } } }
    }
  }
}


PostOperation Fields UsingPost MagSta_a {
  Print[ b, OnElementsOf Region[ {Vol_Mag} ],
	 LastTimeStepOnly, File "b.pos"] ;
  Echo[ Str["l=PostProcessing.NbViews-1;",
	    "View[l].ArrowSizeMax = 100;",
	    "View[l].CenterGlyphs = 1;",
	    "View[l].GlyphLocation = 1;",
	    "View[l].RangeType = 1;",
	    "View[l].SaturateValues = 1;",
	    "View[l].CustomMax = 2;",
	    "View[l].CustomMin = 1e-06;",
	    "View[l].LineWidth = 2;",
	    "View[l].VectorType = 4;" ] ,
  	File "tmp.geo", LastTimeStepOnly] ;
}

PostOperation Check_Periodicity UsingPost MagSta_a {
  If(Gauge == 1)
    // Print the tree for debugging purposes.
	// The group to print is the ExtendedGroup of type EdgesOfTreeIn 
	// automatically generated by getDP at Pre-Processing.
	// The group name name (here '_CO_Entity_39') is indicated in the message console.
    PrintGroup[ _CO_Entity_39 , In Vol_Tree, File "Tree.pos"];

    Echo[ Str["l=PostProcessing.NbViews-1;",
              "View[l].ColorTable = { DarkRed };",
              "View[l].LineWidth = 5;"] ,
            File "tmp.geo", LastTimeStepOnly] ;
  EndIf
	/*
  Print[ bsurf, OnElementsOf Region[ {Sur_SlidingMaster} ],
	 LastTimeStepOnly, File "bsm.pos"] ;
  Echo[ Str["l=PostProcessing.NbViews-1;",
	    "View[l].ArrowSizeMax = 100;",
	    "View[l].CenterGlyphs = 0;",
	    "View[l].GlyphLocation = 1;",
	    "View[l].ScaleType = 1;",
	    "View[l].LineWidth = 3;",
	    "View[l].VectorType = 4;" ] ,
  	File "tmp.geo", LastTimeStepOnly] ;

  Print[ bsurf, OnElementsOf Region[ {Sur_SlidingSlave} ],
	 LastTimeStepOnly, File "bss.pos"] ;
  Echo[ Str["l=PostProcessing.NbViews-1;",
	    "View[l].ArrowSizeMax = 100;",
	    "View[l].CenterGlyphs = 0;",
	    "View[l].GlyphLocation = 1;",
	    "View[l].ScaleType = 1;",
	    "View[l].LineWidth = 3;",
	    "View[l].VectorType = 4;" ] ,
  	File "tmp.geo", LastTimeStepOnly] ;
	*/
  Print[ bsurf, OnElementsOf Region[ Sur_Link ],
  	 LastTimeStepOnly, File "bsl.pos"] ;
  Echo[ Str["l=PostProcessing.NbViews-1;",
  	    "View[l].ArrowSizeMax = 100;",
  	    "View[l].CenterGlyphs = 0;",
  	    "View[l].GlyphLocation = 1;",
  	    "View[l].ScaleType = 1;",
  	    "View[l].LineWidth = 3;",
  	    "View[l].VectorType = 4;" ] ,
  	File "tmp.geo", LastTimeStepOnly] ;
}


