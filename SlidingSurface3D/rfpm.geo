Include "rfpm_common.pro";

SetFactory("OpenCASCADE");

Mesh.CharacteristicLengthMin = lc;
Mesh.CharacteristicLengthMax = lc;
tol = 1e-6;

AnglePole = (ModelAngleMax - ModelAngleMin) * deg;
AngleMagnet = 0.8 * AnglePole;

DefineConstant[
  // radial dimensions
  lRotor = {44, Range{1,100}, Step 1,
    Name "}Dimensions/Rotor/Radius [mm]"}
  lMagnetIn = {10, Range{0,100}, Step 1,
    Name "}Dimensions/Magnet/In-rotor thickness [mm]"}
  lMagnetOut = {5, Range{0,100}, Step 1,
    Name "}Dimensions/Magnet/Out-of-rotor thickness [mm]"}
  lGapRotor = {7, Range{1,100}, Step 1,
    Name "}Dimensions/Rotor/Gap thickness [mm]"}
  lGapStator = {3, Range{1,100}, Step 1,
    Name "}Dimensions/Stator/Gap thickness [mm]"}
  lAirRing = {1, Range{1,100}, Step 1,
    Name "}Dimensions/Stator/Air ring thickness [mm]"}
  lStator = {30, Range{0.1,100}, Step 0.1,
    Name "}Dimensions/Stator/Thickness [mm]"}
  lInf = {30, Range{1,100}, Step 1,
    Name "}Dimensions/Stator/Outside thickness [mm]"}
  // heights
  hRotor = {80, Range{1,100}, Step 1,
    Name "}Dimensions/Rotor/Height [mm]"}
  hMagnet = {40, Range{1,100}, Step 1,
    Name "}Dimensions/Magnet/Height [mm]"}
  hTot = {130, Range{1,100}, Step 1, Closed 1,
    Name "}Dimensions/Total height [mm]"}
];

lRotor *= mm;
lMagnetIn *= mm;
lMagnetOut *= mm;
lGapRotor *= mm;
lGapStator *= mm;
lAirRing *= mm;
lStator *= mm;
lInf *= mm;
hRotor *= mm;
hMagnet *= mm;
hTot *= mm;

// initial rotor bloc
Cylinder(1) = {0,0,0, 0,0,hRotor, lRotor, AnglePole};

// magnet
Cylinder(2) = {0,0,0, 0,0,hMagnet, lRotor + lMagnetOut, AngleMagnet};
Cylinder(3) = {0,0,0, 0,0,hMagnet, lRotor - lMagnetIn, AngleMagnet};
BooleanDifference(4) = { Volume{2}; Delete; }{ Volume{3}; Delete; };
Rotate{ {0,0,1}, {0,0,0}, (AnglePole - AngleMagnet) / 2 }{ Volume{4}; }
Physical Volume("Magnet", 2) = 4;

// rotor
BooleanDifference(5) = { Volume{1}; Delete; }{ Volume{4}; };
Physical Volume("Rotor", 1) = 5;

// rotor air
Cylinder(6) = {0,0,0, 0,0,hTot, lRotor + lGapRotor, AnglePole};
BooleanDifference(7) = { Volume{6}; Delete; }{ Volume{4, 5}; };
Physical Volume("AirRotor", 4) = 7;

// make rotor parts conformal
VolumesRotor() = {4,5,7};
BooleanFragments{ Volume{VolumesRotor()}; Delete; }{}

// air ring
Cylinder(8) = {0,0,0, 0,0,hTot, lRotor + lGapRotor , AnglePole};
Cylinder(9) = {0,0,0, 0,0,hTot, lRotor + lGapRotor + lAirRing, AnglePole};
BooleanDifference(10) = { Volume{9}; Delete; }{ Volume{8}; Delete; };
Physical Volume("AirRing", 6) = 10;

// stator
Cylinder(11) = {0,0,0, 0,0,hRotor, lRotor + lGapRotor + lGapStator, AnglePole};
Cylinder(12) = {0,0,0, 0,0,hRotor, lRotor + lGapRotor + lGapStator + lStator, AnglePole};
BooleanDifference(13) = { Volume{12}; Delete; }{ Volume{11}; Delete; };
Physical Volume("Stator", 3) = 13;

// stator air
Cylinder(14) = {0,0,0, 0,0,hTot, lRotor + lGapRotor + lAirRing, AnglePole};
Cylinder(15) = {0,0,0, 0,0,hTot, lRotor + lGapRotor + lGapStator + lStator + lInf, AnglePole};
BooleanDifference(16) = { Volume{15}; Delete; }{ Volume{14}; Delete; };
BooleanDifference(17) = { Volume{16}; Delete; }{ Volume{13}; };
Physical Volume("AirStator", 5) = 17;

// make stator parts and air ring conformal - but don't glue them to the rotor:
// we need a "double" surface to act as "SlidingMaster" and "SlidingSlave",
// which will be matched programmatically with a Link constraint in getdp after
// rotating the rotor alone
VolumesStator() = {10,13,17};
BooleanFragments{ Volume{VolumesStator()}; Delete; }{}

// We're done with the volumes. Due to internal relabelling during
// BooleanFragments that cannot be controlled by the user, the consistent way to
// identify the surface tags is to proceed geometrically. The surface tags that
// are needed are in general pieces of the outer boundary of the solid where
// specific boundary conditions are to be imposed.  Very often, the volume
// occupied by a FE model is a parallelepipedic block in some coordinate system
// (Cartesian, cylindrical, ...).  The boundary conditions are thus associated
// with the faces of that block and can therefore be identified by means of thin
// bounding boxes in the coordinate space.

Function ChangeCoordinates
  // return coordinates of a point depending on CoordinateSystem
  //
  // input: point() and CoordinateSystem
  // output: coord()
  If(CoordinateSystem == 1) // cylindrical coordinates
    coord() = { Hypot(point(0), point(1)), Atan2(point(1), point(0)), point(2) };
  Else // Cartesian coordinates
    coord() = point();
  EndIf
Return

Function Volume2Cube
  // return the parallelepipedic coordinate block of a list of volumes
  //
  // input: Volumes()
  // output: cube()
  _points() = PointsOf{ Volume{ Volumes() }; };
  point() = Point { _points(0) };
  Call ChangeCoordinates;
  cube() = {coord(0), coord(1), coord(2), coord(0), coord(1), coord(2)};
  For p In {1 : #_points() - 1}
    point() = Point{ _points(p) };
    Call ChangeCoordinates;
    cube(0) = (coord(0) < cube(0) ? coord(0) : cube(0));
    cube(1) = (coord(1) < cube(1) ? coord(1) : cube(1));
    cube(2) = (coord(2) < cube(2) ? coord(2) : cube(2));
    cube(3) = (coord(0) > cube(3) ? coord(0) : cube(3));
    cube(4) = (coord(1) > cube(4) ? coord(1) : cube(4));
    cube(5) = (coord(2) > cube(5) ? coord(2) : cube(5));
  EndFor
Return

Function Cube2Face
  // return the list of surfaces of the model that have all their points located
  // inside a thin box encosing the FaceId'th face of that coordinate block
  //
  // input: cube() and FaceId
  // output: face()
  _bbox() = cube();
  _bbox(Modulo(FaceId + 3, 6)) = _bbox(FaceId);
  face() = {};
  _surfaces() = Abs(Boundary{ Volume{ Volumes() }; });
  For s In { 0:#_surfaces()-1 }
    NbIn = 0;
    _points() = PointsOf { Surface { _surfaces(s) } ; };
    For p In { 0 : #_points() - 1 }
      point() = Point { _points(p) };
      Call ChangeCoordinates;
      If((CoordinateSystem == 1) && (coord(0) == 0))
        coord(1) = _bbox(1);
      EndIf
      If((coord(0) > _bbox(0) - tol) &&
         (coord(1) > _bbox(1) - tol) &&
         (coord(2) > _bbox(2) - tol) &&
         (coord(0) < _bbox(3) + tol) &&
         (coord(1) < _bbox(4) + tol) &&
         (coord(2) < _bbox(5) + tol))
        NbIn++;
      EndIf
    EndFor
    If(NbIn == #_points())
      face() += _surfaces(s);
    EndIf
  EndFor
Return

CoordinateSystem = 1; // cylindrical coordinates

// get useful rotor surfaces
Volumes() = VolumesRotor(); Call Volume2Cube;
Printf("cube rotor: ", cube());
FaceId = 1; Call Cube2Face; // FIXME: cannot have statement here
RotorPerMaster() = face();
FaceId = 2; Call Cube2Face;
RotorBottom() = face();
FaceId = 3; Call Cube2Face;
SlidingSlave() = face();
FaceId = 4; Call Cube2Face;
RotorPerSlave() = face();
FaceId = 5; Call Cube2Face;
RotorTop() = face();

// get useful stator surfaces
Volumes() = VolumesStator(); Call Volume2Cube;
Printf("cube stator: ", cube());
FaceId = 0; Call Cube2Face;
SlidingMaster() = face();
FaceId = 1; Call Cube2Face;
StatorPerMaster() = face();
FaceId = 2; Call Cube2Face;
StatorBottom() = face();
FaceId = 3; Call Cube2Face;
StatorOuter() = face();
FaceId = 4; Call Cube2Face;
StatorPerSlave() = face();
FaceId = 5; Call Cube2Face;
StatorTop() = face();

// The width of the air ring region is arbitrary, but it is in general thin,
// especially in machine models where it has to fit into the air gap.

// get boundary surfaces of the air ring by geometric identifiying, in order to
// distinguish lateral surfaces from the top and bottom
Volumes() = {10}; Call Volume2Cube;
surfaces() = {};
FaceId = 0; Call Cube2Face;
surfaces() += face();
FaceId = 1; Call Cube2Face;
surfaces() += face();
FaceId = 2; Call Cube2Face;
surfaces() += face();
FaceId = 3; Call Cube2Face;
surfaces() += face();
FaceId = 4; Call Cube2Face;
surfaces() += face();
FaceId = 5; Call Cube2Face;
surfaces() += face();

lines() = Abs(Boundary { Surface{ SlidingSlave() }; });
For num In { 0:#surfaces()-1 }
  lines() += Abs(Boundary { Surface{ surfaces(num) }; });
EndFor
For num In { 0:#lines()-1 }
  points() = PointsOf { Line { lines(num) }; };
  If(#points() != 2)
    Error("This is unexpected"); Abort;
  EndIf
  ptA() = Point { points(0) };
  ptB() = Point { points(1) };
  If(Fabs(Atan2(ptA(1), ptA(0)) - Atan2(ptB(1), ptB(0))) > tol)
    // line along theta-axis
    Transfinite Line{ lines(num) } = NbrDiv+1;
  ElseIf(Fabs(ptA(2) - ptB(2)) > tol)
    // line along z-axis
    Transfinite Line{ lines(num) } = Fabs(ptA(2) - ptB(2))/lc-1;
  Else
    // line along r-axis
    Transfinite Line{ lines(num) } = 2;
  EndIf
EndFor

Transfinite Surface{ surfaces() } ;
Recombine Surface{ surfaces({1,2,4,5}) };
Transfinite Surface{ SlidingSlave() } ;
Transfinite Volume { 10 };

// Identify 'SlidingSubmaster' Curve

lines() = Abs(Boundary { Surface{ SlidingMaster() }; });
For l In { 0:#lines()-1 }
  points() = PointsOf { Line { lines(l) }; };
  If(#points() != 2)
    Error("This is unexpected"); Abort;
  EndIf
  ptA() = Point { points(0) };
  ptB() = Point { points(1) };
  If(Fabs(Atan2(ptA(1), ptA(0)) - AnglePole) < tol &&
     Fabs(Atan2(ptB(1), ptB(0)) - AnglePole) < tol)
    SlidingSubmaster =  lines(l);
  EndIf
EndFor

// Identify 'SlidingSubslave' curve

lines() = Abs(Boundary { Surface{ SlidingSlave() }; });
For l In { 0:#lines()-1 }
  points() = PointsOf { Line { lines(l) }; };
  If(#points() != 2)
  Error("This is unexpected"); Abort;
  EndIf
  ptA() = Point { points(0) };
  ptB() = Point { points(1) };
  If(Fabs(Atan2(ptA(1), ptA(0)) - AnglePole) < tol &&
     Fabs(Atan2(ptB(1), ptB(0)) - AnglePole) < tol)
    SlidingSubslave =  lines(l);
  EndIf
EndFor

Physical Surface("Outer"          , 10) = {StatorOuter()};
Physical Surface("Top"            , 11) = {RotorTop(), StatorTop()};
Physical Surface("Bottom"         , 12) = {RotorBottom(), StatorBottom()};
Physical Surface("RotorPerMaster" , 13) = {RotorPerMaster()};
Physical Surface("RotorPerSlave"  , 14) = {RotorPerSlave()};
Physical Surface("StatorPerMaster", 15) = {StatorPerMaster()};
Physical Surface("StatorPerSlave" , 16) = {StatorPerSlave()};
Physical Surface("SlidingMaster"  , 17) = {SlidingMaster()};
Physical Surface("SlidingSlave"   , 18) = {SlidingSlave()};

Physical Line("Sliding_Submaster" , 20) = {SlidingSubmaster};
Physical Line("Sliding_Subslave"  , 21) = {SlidingSubslave};

// Ensure identical meshes on the periodic faces
For num In {0:#RotorPerMaster()-1}
  Periodic Surface { RotorPerSlave(num) } =
    { RotorPerMaster(num) } Rotate { {0,0,1}, {0,0,0}, AnglePole };
EndFor
For num In {0:#StatorPerMaster()-1}
  Periodic Surface { StatorPerSlave(num) } =
    { StatorPerMaster(num) } Rotate { {0,0,1}, {0,0,0}, AnglePole };
EndFor

treeLines()  = Abs(CombinedBoundary { Physical Surface { 10 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 11 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 12 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 13 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 14 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 15 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 16 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 17 }; }) ;
treeLines() += Abs(CombinedBoundary { Physical Surface { 18 }; }) ;

Physical Line("TreeLines", 22) = { treeLines() };
