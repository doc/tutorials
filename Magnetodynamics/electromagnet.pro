/* -------------------------------------------------------------------
   Tutorial 7a : magnetic fields of an electromagnet

   Features:
   - Use of a template formulation library
   - Identical to Tutorial 2 for a static current source
   - Frequency-domain or time-domain solution for a time-dependent
     current source

   To compute the static solution in a terminal:
       getdp electromagnet -solve Magnetostatics2D_a -pos sta

   To compute the frequency-domain solution in a terminal:
       getdp electromagnet -solve Magnetodynamics2D_av -pos dyn

   To compute the time-dependent dynamic solution in a terminal:
       getdp electromagnet -setnumber TimeDomain 1 -solve Magnetodynamics2D_av -pos dyn

   To compute the solution interactively from the Gmsh GUI:
       File > Open > electromagnet.pro
       You may choose the Resolution in the left panel:
       Magnetodynamics2D_av (default) or Magnetostatics2D_a
       Run (button at the bottom of the left panel)
   ------------------------------------------------------------------- */

Include "electromagnet_common.pro";

Group {
  // Physical regions
  Air    = Region[ 101 ];   Core   = Region[ 102 ];
  Ind    = Region[ 103 ];   AirInf = Region[ 111 ];
  Surface_ht0 = Region[ 1100 ];
  Surface_bn0 = Region[ 1101 ];
  Surface_Inf = Region[ 1102 ];

  // Abstract regions used in the "Lib_Magnetodynamics2D_av_Cir.pro" template file
  // that is included below:
  Vol_Mag = Region[{Air, Core, Ind, AirInf}]; // full magnetic domain
  Vol_C_Mag = Region[Core]; // massive conductors
  Vol_S_Mag = Region[Ind]; // stranded conductors (coils)
  Vol_Inf_Mag = Region[AirInf]; // ring-shaped shell for infinite transformation
  Val_Rint = rInt; Val_Rext = rExt; // interior and exterior radii of ring

  DefineConstant[
    TimeDomain = {0, Choices{0 = "Frequency-domain", 1 = "Time-domain"},
      Name "Model parameters/03Analysis type"}
    NonLinearCore = {0, Choices{0, 1}, Visible TimeDomain,
      Name "Model parameters/Non linear B-H curve in core"}
  ];
  If(NonLinearCore)
    Vol_NL_Mag = Region[Core];
  EndIf
}

Function {
  DefineConstant[
    Current = {0.01, Name "Model parameters/Max. current [A]"},
    murCore = {1000, Visible !NonLinearCore,
      Name "Model parameters/Mur core"}
    // by default, a linear ramp from 0 to 1 until 10 ms, then a constant value
    // of 1; any desired function can be specified
    timeFunction = {"($Time < TimeFinal / 2) ? (2 / TimeFinal * $Time) : 1",
      Choices{"($Time < TimeFinal / 2) ? (2 / TimeFinal * $Time) : 1",
        "Sin[2 * Pi * Freq * $Time]", "Sin[2 * Pi * 10 * $Time]"},
      Name "Model parameters/04Time function", Visible TimeDomain}

    // overwrite parameters from the "Lib_Magnetodynamics2D_av_Cir.pro"
    // template:
    Freq = {50, Visible !TimeDomain,
      Name "Model parameters/04Frequency [Hz]" }
    DeltaTime = {1e-3,
      Name "Model parameters/05Time step [s]", Visible TimeDomain}
    TimeFinal = {20e-3,
      Name "Model parameters/06Final simulation time [s]", Visible TimeDomain}
  ];

  Parse[StrCat["myModulation[] = ", timeFunction, ";"]];

  // Fix parameters from the "Lib_Magnetodynamics2D_av_Cir.pro" template:
  If(TimeDomain)
    Flag_FrequencyDomain = 0;
  Else
    Flag_FrequencyDomain = 1;
  EndIf

  mu0 = 4.e-7 * Pi;
  nu[ Region[{Air, Ind, AirInf}] ]  = 1. / mu0;

  If(NonLinearCore)
    Mat_core_h() = {
      0.0000e+00, 5.5023e+00, 1.1018e+01, 1.6562e+01, 2.2149e+01, 2.7798e+01, 3.3528e+01,
      3.9363e+01, 4.5335e+01, 5.1479e+01, 5.7842e+01, 6.4481e+01, 7.1470e+01, 7.8906e+01,
      8.6910e+01, 9.5644e+01, 1.0532e+02, 1.1620e+02, 1.2868e+02, 1.4322e+02, 1.6050e+02,
      1.8139e+02, 2.0711e+02, 2.3932e+02, 2.8028e+02, 3.3314e+02, 4.0231e+02, 4.9395e+02,
      6.1678e+02, 7.8320e+02, 1.0110e+03, 1.3257e+03, 1.7645e+03, 2.3819e+03, 3.2578e+03,
      4.5110e+03, 6.3187e+03, 8.9478e+03, 1.2802e+04, 1.8500e+04, 2.6989e+04, 3.9739e+04,
      5.9047e+04, 8.8520e+04, 1.3388e+05, 2.0425e+05, 3.1434e+05, 4.8796e+05, 7.6403e+05
    } ;
    Mat_core_b() = {
      0.0000e+00, 5.0000e-02, 1.0000e-01, 1.5000e-01, 2.0000e-01, 2.5000e-01, 3.0000e-01,
      3.5000e-01, 4.0000e-01, 4.5000e-01, 5.0000e-01, 5.5000e-01, 6.0000e-01, 6.5000e-01,
      7.0000e-01, 7.5000e-01, 8.0000e-01, 8.5000e-01, 9.0000e-01, 9.5000e-01, 1.0000e+00,
      1.0500e+00, 1.1000e+00, 1.1500e+00, 1.2000e+00, 1.2500e+00, 1.3000e+00, 1.3500e+00,
      1.4000e+00, 1.4500e+00, 1.5000e+00, 1.5500e+00, 1.6000e+00, 1.6500e+00, 1.7000e+00,
      1.7500e+00, 1.8000e+00, 1.8500e+00, 1.9000e+00, 1.9500e+00, 2.0000e+00, 2.0500e+00,
      2.1000e+00, 2.1500e+00, 2.2000e+00, 2.2500e+00, 2.3000e+00, 2.3500e+00, 2.4000e+00
    } ;
    Mat_core_b2 = Mat_core_b()^2;
    Mat_core_h2 = Mat_core_h()^2;
    Mat_core_nu = Mat_core_h() / Mat_core_b();
    Mat_core_nu(0) = Mat_core_nu(1);
    Mat_core_nu_b2  = ListAlt[Mat_core_b2(), Mat_core_nu()] ;
    nu_core[] = InterpolationAkima[ SquNorm[$1] ]{ Mat_core_nu_b2() } ;
    dnudb2_core[] = dInterpolationAkima[SquNorm[$1]]{ Mat_core_nu_b2() } ;
    h_core[] = nu_core[$1] * $1 ;
    dhdb_core[] = TensorDiag[1,1,1] * nu_core[$1#1] +
      2*dnudb2_core[#1] * SquDyadicProduct[#1]  ;

    nu[ Core ]  = nu_core[$1];
    dhdb[ Core ] = dhdb_core[$1];
  Else
    nu[ Core ]  = 1. / (murCore * mu0);
  EndIf

  sigma[ Core ] = 1e6 / 100;
  sigma[ Ind ] = 5e7;

  Ns[ Ind ] = 1000 ; // number of turns in coil
  Sc[ Ind ] = SurfaceArea[] ; // area of coil cross section

  // Current density in each coil portion for a unit current (will be multiplied
  // by the actual total current in the coil)
  js0[ Ind ] = Ns[] / Sc[] * Vector[0, 0, -1];

  // For a correct definition of the voltage:
  CoefGeos[] = 1; // planar model, 1 meter thick
}

Constraint {
  { Name MagneticVectorPotential_2D;
    Case {
      { Region Surface_bn0; Value 0; }
      { Region Surface_Inf; Value 0; }
    }
  }
  { Name Current_2D;
    Case {
      If(Flag_FrequencyDomain)
        // Amplitude of the phasor is set to "Current"
        { Region Ind; Value Current; }
      Else
        // Time-dependent value is set to "Current * myModulation[]"
        { Region Ind; Value Current; TimeFunction myModulation[]; }
      EndIf
    }
  }
  { Name Voltage_2D;
    Case {
      { Region Core; Value 0; }
    }
  }
}

Include "Lib_Magnetodynamics2D_av_Cir.pro";

PostOperation {
  { Name dyn; NameOfPostProcessing Magnetodynamics2D_av;
    Operation {
      Print[ az, OnElementsOf Vol_Mag, File "az.pos" ];
      Print[ b, OnElementsOf Vol_Mag, File "b.pos" ];
      Print[ j, OnElementsOf Vol_Mag, File "j.pos" ];
      Print[ I, OnRegion Ind, File "I.txt", Format TimeTable,
        SendToServer "Output/Imposed current" ];
    }
  }
  { Name sta; NameOfPostProcessing Magnetostatics2D_a;
    Operation {
      Print[ az, OnElementsOf Vol_Mag, File "az.pos" ];
      Print[ b, OnElementsOf Vol_Mag, File "b.pos" ];
      Print[ j, OnElementsOf Vol_Mag, File "j.pos" ];
    }
  }
}

// Choose resolution, computation and post-operation in interactive mode
DefineConstant [
  R_ = {"Magnetodynamics2D_av", Name "GetDP/1ResolutionChoices", Visible 0},
  C_ = {"-solve -pos -bin", Name "GetDP/9ComputeCommand", Visible 0},
  P_ = {"dyn", Name "GetDP/2PostOperationChoices", Visible 0}
];
