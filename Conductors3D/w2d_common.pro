
mm = 1e-3;
deg = 180/Pi;

DefineConstant[
  NumWires = {1, Name "Parameters/0Number of wires", 
			  Choices {1="1", 2="2", 3="3"}, Visible 1}

  Flag_Thin = {1, Name "Parameters/1Wire representation", 
			   Choices {0="Real", 1="1D (raw sleeve)", 2="1D (regular sleeve)", 3="1D (naive)"}}
  Flag_3D   = {0, Name "Parameters/2Extruded model", Choices {0,1}, Visible 0}
  Flag_Maps = {1, Name "Input/2Display maps", Choices {0,1}, Visible 1}
  LogFreq = {4,  Min 0, Max 8, Step 1, Name "Input/4Log of frequency"}
  Freq = 10^LogFreq

  WireRadius   = {0.564189, Name "Parameters/5Wire radius [mm]"}
  MeshSizeWire = {0.2, Name "Parameters/6Mesh size on wire [mm]"}
  SleeveRadius = {2, Name "Parameters/7Sleeve radius [mm]"}
  box = {100*mm, Name "Parameters/7box half-width [m]"}

  rw = WireRadius*mm 
  rs = SleeveRadius*mm
  A_c = Pi*rw^2 // wire cross section
  rout = box
  Lz = 1*mm

  /* 'WireRadius' is the actual radius of the wire.
     'MeshSizeWire' is the imposed mesh size at the nodes of the wire,
     and thus also the practical approximative value of the radius of the sleeve.
     The 'rs' and 'rw' parameters are assumed identical for all wires.
   */
];



Xlocs() = { 0, 5*mm, -5*mm};
Ylocs() = { 0, 0, 0};
For i In {1:NumWires}
  DefineConstant[
	WX~{i} = { Xlocs(i-1), // place wires symmetrically around x=0
			   Name Sprintf("Parameters/Wire %g/0X position [m]", i), Closed }
    WY~{i} = { Ylocs(i-1), Name Sprintf("Parameters/Wire %g/0Y position [m]", i) }
    WI~{i} = { 1, Name Sprintf("Parameters/Wire %g/2Current [A]", i) }
    WP~{i} = { 0*NumWires*(i-1),
      Name Sprintf("Parameters/Wire %g/3Phase [deg]", i) }
    ];
EndFor
